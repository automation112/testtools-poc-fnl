from selenium import webdriver
import os
from halo import Halo
import time, bcolors
from testTools.testLab import testLab
import re
from selenium.webdriver.chrome.options import Options # run_tests_on_existing_browser

class Create_TestCase(testLab):
    # url = "www.WHODIS.com"
    def __init__(self, testDefinitionsDir, web_app_url ="www.TBD.com", execModulesList=None, testSuit=None, appName=None):
        self.url = web_app_url
        super().__init__(testDefinitionsDir=testDefinitionsDir, execModulesList=execModulesList, testSuit=testSuit, applicationName=appName)

    @Halo(text=f'{bcolors.WAITMSG}Loading WebPage{bcolors.END}', spinner='...', animation='marquee', placement='right')
    def setUp_Browser(self):
        if 'nt' in os.name.lower():
            # import psutil
            # for proc in psutil.process_iter():
            #     if proc.name() == "chromedriver.exe":
            #         proc.kill()
            time.sleep(1)
            caps = webdriver.DesiredCapabilities.CHROME.copy()
            caps['goog:loggingPrefs'] = {'performance': 'ALL'}
            options = webdriver.ChromeOptions()
            options.add_argument("--start-maximized")
            options.add_argument('ignore-certificate-errors')
            options.add_argument("--incognito")

    # run_tests_on_existing_browser
            opt = Options()
            opt.add_experimental_option("debuggerAddress", "localhost:8989")

            caps['acceptInsecureCerts'] = True
            import pathlib
            projectDir = str(pathlib.Path(__file__).parent.absolute()) + "\\Resources\\Drivers\\"
            executable_path = os.path.join(projectDir, "chromedriver.exe")

    # run_tests_on_existing_browser _ added chrome_options=opt
            self.browser = webdriver.Chrome(executable_path=executable_path, options=options, desired_capabilities=caps)

            # self.browser.get(setUpBrowser.utils.getTempValue('URL')['dex'])
            spinner = Halo(text='Loading', spinner='dots')
            spinner.start()
            time.sleep(3)
            print(self.url)
            self.browser.get(url=self.url)
            spinner.stop()
            spinner.clear()
            self.browser.implicitly_wait(15)
            Create_TestCase.utils.__init__(browser=self.browser)
            time.sleep(4)
        else:
            import undetected_chromedriver as uc
            uc.install()
            from selenium.webdriver import Chrome, ChromeOptions
            options = ChromeOptions()
            options.headless = True
            # user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
            user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)'
            options.binary_location = '/usr/bin/google-chrome'
            options.add_argument('no-sandbox')
            options.add_argument('headless')
            options.add_argument('disable-dev-shm-usage')
            options.add_argument(f'user-agent={user_agent}')
            options.add_argument("window-size=1920,1080")
            options.add_argument("--incognito")
            self.browser = Chrome(options=options)
            self.browser.get(Create_TestCase.utils.getTempValue('URL')['dex'])
            print(self.browser.page_source)
            time.sleep(10)
            Create_TestCase.utils.__init__(browser=self.browser)

    def setUp_dockerEnvironment(self):
        if os.name == 'posix':
            if os.environ['ENV'] == 'Jenkins':
                self.utils.setTempValue('URL', self.url)
            else:
                self.utils.setTempValue('URL', self.url)
                self.applicationName = os.environ['ENV']
        else:
            self.utils.setTempValue('URL', self.url)

    def start_test_execution(self):
        self.suitRunner(browser=self.browser)

    def closeBrowser(self):
    # run_tests_on_existing_browser
        pass
        # self.browser.close()

if __name__ == "__main__":

    testDefinitions = r"\\vffs\RO\atp2$\VFRO_Automation\CTDA_Exports\MTV330-VR14171_14172-subscriberConfig\CTDA_Export\MTV330-VR14171_14172-subscriberConfig.txt"

    hardcodedTest = \
    [
        {
            "test1": [
                {"page_Login": {
                    'channel': 'RETAIL',
                }
                },
                {"page_Search": {
                    "searchType": "MSISDN",
                    "searchValue": "724220342"
                }
                },
                {"page_Dashboard": {
                    'configType': '_ACHIZITIE',
                    'abNouType': 'GA',
                    'scenarioNameDC': 'CR_True_Client_Matur',
                }
                },
                {"page_Configureaza": {
                    "pageConfigureaza_offerType": "serviceOnly",
                    "pageConfigureaza_viewMore": "False",
                    "pageConfigureaza_offerName": "regularOffer",
                    "pageConfigureaza_addOns": "changeSelection",
                    "pageConfigureaza_acquisitionType": "Portin postpaid",
                    "pageConfigureaza_cartScenario": "Continue button",
                }
                },
                {"page_CustomerDetails": {
                    "pageCustomerDetails_acquisitionType": "Portin postpaid",
                    "pageCustomerDetails_portinType": "temporary",
                    "pageCustomerDetails_tempType": "Yes",
                    "pageCustomerDetails_temporary_phoneType": "Customized",
                    "pageCustomerDetails_existingNotifNo": "True",
                    "pageCustomerDetails_notifAction": "X",
                    "pageCustomerDetails_userType": "Yes",
                    "pageCustomerDetails_newBilling": "Yes",
                    "pageCustomerDetails_existingStreet": "No",
                    "pageCustomerDetails_invoiceType": "Electronica",
                    "pageCustomerDetails_cancelOrder": "No",
                }
                },
                {"page_ShippingMethods": {
                    "pageShippingMethods_existingSIM": "No",
                }
                },
                {"page_OrderSummary": {
                    "pageOrderSummary_userActions": "Yes",
                    "pageOrderSummary_viewSummary": "No",
                    "pageOrderSummary_GDPR": "noVDF_yesPartners",
                }
                },
                {"page_UploadDocuments": {
                    "pageUploadDocuments_docOpt": "firstOption",
                    "pageUploadDocuments_IDconsent": "yes",
                    "pageUploadDocuments_signatureMethod": "Yes",
                    "pageUploadDocuments_electronicMethod": "Digital Paper"
                }
                }
            ]
        }

    ]
    testDefinitions = hardcodedTest
    dexEnv = "dexuat1"
    url1 = "https://dexuat1.aws.connex.ro/retail/login"
    url2 = "https://dexuat2.aws.connex.ro/retail/login"

    url = url2

    targetTestDir = 'CBU_F_ACQ'
    Create_TestCase(testSuit=testDefinitions,
                    testDefinitionsDir=targetTestDir,
                    web_app_url=url,
                    # execModulesList='page_Configureaza',
                    appName=re.search('//(.*)\.aws', url).group(1),
                    #execModulesList=['page_Dashbo']
                    )

