dexEnv="dexuat2"

def get_endPoint(env, value):
        environment = {'dexuat1': {"CM_ENV": "CM30", "EAI":"EAIUAT5", "URL": "https://dexuat1.aws.connex.ro/retail/login", "PEGA_ENV": "PEGA_UAT3"},
                       'dexuat2': {"CM_ENV": "CM20", "EAI":"EAIUAT6", "URL": "https://dexuat2.aws.connex.ro/retail/login", "PEGA_ENV": "PEGA_UAT2"}}
        return environment[env][value]


