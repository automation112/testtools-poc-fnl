from testTools.utilities import utilities as utils
#from dex_old.CBU_F_ACQ.PersTestModules import sebid
from random import randint
import time


class page_Search():

    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary=inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)
        self.searchValue = self.utils.get_Child(paramName='searchValue')
        self.searchType = self.utils.get_Child(paramName='searchType')
        self.testData = testData
        #self.sebi = sebid.testFunctions_Search(self.utils)
        self.searchDict = {'MSISDN': "//span[contains(text(),'MSISDN')]",
                           'searchKey_MSISDN': '//input[@name="search"]',
                           'cauta': "//button[@data-automation-id='search-button']",
                           'CNP': "//span[contains(text(),'CNP')]",
                           'searchKey_CNP': '//input[@name="search"]',
                           'CONT_CLIENT': "//span[contains(text(),'CONT CLIENT')]",
                           'searchKey_CONT_CLIENT': '//input[@name="search"]',
                           'searchKey_ADDRESS': '//input[@name="search"]',
                           'ADDRESS': "//span[contains(text(),'INSTALARE')]",
                           'Abonament_nou': "//div[@data-automation-id='new-subscriber-link']",
                           'Numar_Nou(GA)': '//li[@data-automation-id="tab-tab-key-0"]',
                           'Credit_Rate_Da': '//label[1]//*[@name="isCustomerConsent"]',
                           'Credit_Rate_Nu': '//label[2]//*[@name="isCustomerConsent"]',
                           'Numar_Contact': '//input[@name="contactNumber"]',
                           'continua': "//button[contains(@class,'primary bold right-align')]",
                           'Portare': '//li[@data-automation-id="tab-tab-key-1"]',
                           'Numar_Portat': '//input[@name="portedNumber"]',
                           'Donor': '//select[@name="provider"]',
                           'Numar_Contact_Portare': "//div[2]/div[@class='fields-wrap' and 1]/div[1]/input[1]",
                           'POSTPAID': '//input[@name="portedNumberType" and @value="postpaid"]',
                           'Prepaid': '//input[@name="portedNumberType" and @value="prepaid"]',
                           'Migrare_de_la_Prepaid': "//li[@data-automation-id='tab-tab-key-2']",
                           'Number_To_Migrate': '//input[@name="numberToMigrate"]',
                           'Trimtie Token': "//span[contains(text(),'TRIMITE TOKEN')]",
                           'Introdu Token': "//input[@placeholder='Introdu TOKEN']",
                           'Valideaza Token': """//a[@data-automation-id="token-consent-validate-token"]""",
                           'Factura_1': "//input[@name='invoice1']",
                           'Factura_2': "//input[@name='invoice2']",
                           'Factura_3': "//input[@name='invoice3']",
                           'adauga': "//button[contains(@class,'bold nbo-button')]",
                           'BundleBannerOffer': "//span[contains(text(),'Împreuna cu')]",
                           '360_OferteMobile': """//span[contains(text(),'OFERTE MOBILE')]""",
                           '360_Doar_Servicii': "//span[contains(text(),'DOAR SERVICII')]"
                           }


    def test_page_Search(self):

        if self.searchType == 'MSISDN':

            #msisdn = '722848655'
            #time.sleep(3)
            # self.utils.click_js(self.searchDict['MSISDN'])
            # brokenMsisdn = randint(100, 999999999)
            # self.utils.setTempValue('brokenMsisdn', brokenMsisdn)
            # self.utils.type_js(self.searchDict['searchKey_MSISDN'], brokenMsisdn)
            # self.utils.click_js(self.searchDict['cauta'])
            # # for i in self.searchValue.split():
            #     input_msisdn = page_search.utils.type_js(self.searchDict['searchKey_MSISDN'], i)
            #     page_search.utils.click_js(self.searchDict['cauta'])
            #     print(i)
            #     if page_search.utils.element_exists(self.searchDict['cauta']) == False:
            #         print(i)
            self.utils.type_js(self.searchDict['searchKey_MSISDN'], self.searchValue)
            self.utils.click_js(self.searchDict['cauta'])

            self.utils.wait_loadingCircle()

        elif self.searchType == 'CNP':
            self.utils.click_js(self.searchDict['CNP'])
            self.utils.type_js(self.searchDict['searchKey_CNP'], self.searchValue)

        elif self.searchType == 'CONTCLIENT':
            self.utils.click_js(self.searchDict['CONT_CLIENT'])
            self.utils.type_js(self.searchDict['searchKey_CONT_CLIENT'], self.searchValue)

        elif self.searchType == 'ADRESA':
            self.utils.click_js(self.searchDict['ADDRESS'])
            self.utils.type_js(self.searchDict['searchKey_ADDRESS'], self.searchValue)

        else:
            print('No input')



