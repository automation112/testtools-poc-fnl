from testTools.utilities import utilities as utils
from dex_old.CBU_F_ACQ.PersTestModules import sebas


class page_OrderSummary():

    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary=inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)

        self.pageOrderSummary_userActions = self.utils.get_Child(paramName='pageOrderSummary_userActions')
        self.pageOrderSummary_viewSummary = self.utils.get_Child(paramName='pageOrderSummary_viewSummary')
        self.pageOrderSummary_GDPR = self.utils.get_Child(paramName='pageOrderSummary_GDPR')
        self.pageConfigureaza_offerType = self.utils.get_Child(paramName='pageConfigureaza_offerType')

        self.testData = testData
        self.sebas = sebas.testFunctions_OrderSummary(self.utils)


    def test_page_OrderSummary(self):

        # 1._OK(Pb pe DEX1) Error after "Trimite pe Email _ Trimite" - Ne pare rău, a apărut o problema...
        # 2. "Plata unica" - cu ce se compara? - taxa alocare numar?
        # 3. "Lunar" - total?
        # 4. "Oferta servicii" - nume oferta din pgConfigureaza
        # 5. "Promotional benefits"
        # 6. "FMC discount" - lipsa oferte
        # 7. "Costuri suplimentare"
        # 8. OfferName este de forma "RED 9 Credit Rate 0" iar abonamentul ales "RED 9 SIM Only"

        # !!! FARA RESURSE PT SITUATIILE:
        ### Promotional benefits
        ### FMC discount

        self.utils.log_customStep(key='WARN! Wrong value on "Plata Unica", Defect ID # 4385', status='FAIL')
        self.utils.log_customStep(key='WARN! No Resources for "Promotional benefits"/"FMC discount"/"Costuri suplimentare"', status='FAIL')

        self.sebas.page_summary_checks(pageConfigureaza_offerType=self.pageConfigureaza_offerType)

        if self.pageOrderSummary_userActions == 'No':
            pass
        elif self.pageOrderSummary_userActions == 'Yes':
            self.sebas.dotsActions()
            if self.pageOrderSummary_viewSummary == 'No':
                pass
            elif self.pageOrderSummary_viewSummary == 'Yes':
                self.sebas.showDocFromPopUp()
            self.sebas.sendEmail()

        self.sebas.summaryChecks()

        self.sebas.GDPRActions(pageOrderSummary_GDPR=self.pageOrderSummary_GDPR)

        self.sebas.sendOptions()

if __name__ == '__main__':
    print('ok')
