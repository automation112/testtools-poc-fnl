import difflib
import re
from Resources.endPoints import get_endPoint
from Resources import endPoints
import time
class testFunctions_Login:
    def __init__(self, utils):
        self.utils = utils
        self.pageLogin_prerequisite = self.utils.get_Child(paramName='pageLogin_prerequisite')

    def loginActions(self, prereq=None):

        self.utils.click_js(xpath="""//span[contains(text(),'OK')]""", element_name='OK')
        print(prereq, "-------------------")
        self.utils.type_js(xpath="//input[@type='text']", value=prereq['Username'],  # value='mihaela.filip1'
                           element_name="UserName")
        self.utils.type_js(xpath="//input[@type='password']", value="123", element_name="Password")
        self.utils.click_js(xpath="//button[@type='submit']", element_name="Login")
        if not "tls" in self.pageLogin_prerequisite.lower().split(" - ")[0].replace("[", ""):
            if self.utils.titleContains(title='retail/searchCustomer'):
                pass
                self.utils.log_customStep(key='Single Dealer Code', status="PASSED")
            else:
                self.utils.log_customStep(key='Single Dealer Code', status="FAIL")
                self.utils.click_js(xpath="//button[@type='submit']", element_name="Continua")
        else:
            print(self.pageLogin_prerequisite.lower().split(" - ")[0].replace("[", ""), "------------")
            self.utils.click_js(xpath="//button[@data-automation-id='dealer-login-button']", element_name="Continua")

    def invalidCredentials(self, prereq, dealerCode):
        self.utils.click_js(xpath="""//span[contains(text(),'OK')]""", element_name='OK')
        self.utils.type_js(xpath="//input[@type='text']", value=prereq['USERNAME'], element_name="UserName")
        self.utils.type_js(xpath="//input[@type='password']", value=prereq['password'], element_name="Password")
        self.utils.click_js(xpath="//button[@type='submit']")

        if self.utils.element_exists("//span[contains(text(),'Nume utilizator sau Parolă incorectă.')]"):
            self.utils.log_customStep(key='User/Password Invalid message Displayed', status="PASSED")
        else:
            self.utils.log_customStep(key='User/Password Invalid message Displayed', status="FAIL")

        if self.utils.element_exists("//h6[@class='bold']") and dealerCode != 'singleDealerCode':
            self.utils.log_customStep(key='Dealer Option was not displayed', status="PASSED")
        else:
            self.utils.log_customStep(key='Dealer Option was not displayed', status="FAIL")

        if self.utils.titleContains(title='retail/searchCustomer'):
            self.utils.log_customStep(key='User wasn\'t redirected to Search Page', status="PASSED")
        else:
            self.utils.log_customStep(key='User wasn\'t redirected to Search Page', status="FAIL")


class testFunctions_Search:
    def __init__(self, utils, xpaths_Dictionary=None):
        self.utils = utils
        self.xpaths_Dictionary = xpaths_Dictionary
        self.pageSearch_clientConsent = self.utils.get_Child(paramName='pageSearch_clientConsent')
        self.pageConfigureaza_acquisitionType = self.utils.get_Child(paramName='pageConfigureaza_acquisitionType')

    def MSISDN(self, prereq):
        self.utils.element_exists('//div[@class="tabSm bold active" and @data-automation-id="msisdn-button"]',
                                  element_name='MSISDN')
        self.utils.click_js(xpath='//div[@data-automation-id="msisdn-button"]', element_name='MSISDN')
        self.utils.type_js(xpath='//input[@name="search"]',
                           # value='724343808',
                           value=prereq['Prepaid_MSISDN'],
                           element_name='Search_Field', enter=True)
        self.utils.wait_loadingCircle(enable=True)
        # time.sleep(5)

    def CNP(self, prereq):
        self.utils.click_js("//span[contains(text(),'CNP')]", element_name='CNP')
        self.utils.type_js(xpath='//input[@name="search"]',
                           value='2920618015820',
                           # value=prereq['SEARCHVALUE'],
                           element_name='Search_Field', enter=True)
        self.utils.wait_loadingCircle(enable=True)

    def newCustomer(self, prereq):
        # tokenId = self.utils.retryAPI(API='Token',
        #                               URL='http://devops03.connex.ro:8030/getSMSToken/',
        #                               KEY=self.utils.getTempValue('brokenMsisdn'))
        self.utils.click_js("//span[contains(text(),'CNP')]", element_name='CNP')
        # self.utils.type_js(xpath='//input[@name="search"]',
        #                    value=prereq['SEARCHVALUE'], element_name='Search_Field', enter=True)
        self.utils.type_js(xpath='//input[@name="search"]',
                           value=prereq['SEARCHVALUE'], element_name='Search_Field', enter=True)
        # while(not (self.utils.element_isClickable(xpath="//button[@data-automation-id='new-wireless-btn']"))):
        #     time.sleep(1)
        self.utils.click_js("//button[@data-automation-id='new-wireless-btn']", element_name="Client Nou",
                            customTimeout=5)
        if (self.utils.element_exists(xPath="//div[@class='title-popup undefined title-height']")):
            self.utils.log_customStep(key='The Consent pop-up is opened', status="PASSED")
        else:
            self.utils.log_customStep(key='The Consent pop-up is opened', status="FAILED")
        print(self.utils.get_text(xPath="//h3"), "---------")
        if re.sub(r'\s+', ' ', self.utils.get_text(xPath="//h3")) == str(
                ("ABONAMENT NOU | CNP " + prereq['SEARCHVALUE'])):
            self.utils.log_customStep(
                key='The Header of the pop-up contains the following title: ABONAMENT NOU | CNP [cnp_used]',
                status="PASSED")
        else:
            self.utils.log_customStep(
                key='The Header of the pop-up contains the following title: ABONAMENT NOU | CNP [cnp_used]',
                status="FAILED")
        if self.pageConfigureaza_acquisitionType == "generalAquisition":
            if self.utils.element_exists(
                    xPath="//li[@class='tab bold active ' and @data-automation-id='tab-tab-key-0']"):
                self.utils.log_customStep(key='The NUMAR NOU tab is selected by default', status="PASSED")
            else:
                self.utils.log_customStep(key='The NUMAR NOU tab is selected by default', status="FAILED")
            if self.utils.element_exists(xPath="//img[@class='x close-position']") and self.utils.element_exists(
                    xPath="//button[@data-automation-id='subscribers-popup-cancel-button']"):
                self.utils.log_customStep(key='The X and ANULEAZA buttons are visible and accessible to the user',
                                          status="PASSED")
            else:
                self.utils.log_customStep(key='The X and ANULEAZA buttons are visible and accessible to the user',
                                          status="FAILED")
            self.utils.wait_loadingCircle()
            # if  self.utils.element_isClickable(xpath='//button[@data-automation-id="subscribers-popup-continue-button"]'):
            #    self.utils.log_customStep(key='The URMATOARELE button is disabled by default',
            #                            status="PASSED")
            # else:
            #   self.utils.log_customStep(key='The URMATOARELE button is disabled by default',
            #                           status="FAILED")
            if not self.utils.is_element_Selected(
                    xPath="//input[@data-automation-id='agree-customer-consent']") and not self.utils.is_element_Selected(
                    xPath="//input[@data-automation-id='decline-customer-consent']"):
                self.utils.log_customStep(
                    key='The DA and NU buttons in the Client Consent section are unselected by default',
                    status="PASSED")
            else:
                self.utils.log_customStep(
                    key='The DA and NU buttons in the Client Consent section are unselected by default',
                    status="FAILED")
            if self.utils.element_exists(xPath="//span[@class='bold' and span='IMPORTANT: ']"):
                self.utils.log_customStep(
                    key='An IMPORTANT message is displayed to the user, informing that the Client Consent impacts the service acquisition',
                    status="PASSED")
            else:
                self.utils.log_customStep(
                    key='An IMPORTANT message is displayed to the user, informing that the Client Consent impacts the service acquisition',
                    status="FAILED")
            if self.utils.get_attrValue(xPath="//input[@name='contactNumber']", attrName="maxlength") == "9":
                self.utils.type_js(xpath="//input[@name='contactNumber']", value="777777777",
                                   element_name="NUMAR CONTACT")
                self.utils.log_customStep(
                    key='The user is able to capture a 9 digits number in the user input field', status="PASSED")
            else:
                self.utils.log_customStep(
                    key='The user is able to capture a 9 digits number in the user input field', status="FAILED")
            if self.pageSearch_clientConsent == "agree":
                self.utils.click_js(xpath="//input[@data-automation-id='agree-customer-consent']", element_name="DA")
                self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']",
                                    element_name="Trimite Token")
                self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=self.utils.retryAPI(API='Token',
                                                                                                            URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                                                            KEY=self.utils.getTempValue(
                                                                                                                'brokenMsisdn')))
                self.utils.click_js(xpath="//a [@data-automation-id='token-consent-validate-token']",
                                    element_name="Valideaza")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
                self.utils.type_js(xpath="//input[@data-automation-id='PRENUME']", value="test")
                self.utils.type_js(xpath="//input[@data-automation-id='NUME']", value="test")
                self.utils.type_js(xpath="//input[@aria-activedescendant='react-select-3--value']", value="dambovita",
                                   forLoopPause=0.5, enter=True)
                self.utils.type_js(xpath="//input[@aria-activedescendant='react-select-4--value']", value="targoviste",
                                   forLoopPause=0.5, enter=True)
                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-5--value"]', value="aleea",
                                   forLoopPause=0.5, enter=True)
                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-6--value"]',
                                   value="Trandafirilor", forLoopPause=0.5, enter=True)
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Continua")
            elif self.pageSearch_clientConsent == "disagree":
                self.utils.click_js(xpath="//input[@data-automation-id='decline-customer-consent']", element_name="NU")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
                self.utils.type_js(xpath="//input[@data-automation-id='PRENUME']", value="test")
                self.utils.type_js(xpath="//input[@data-automation-id='NUME']", value="test")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Continua")
        if self.pageConfigureaza_acquisitionType == "Portin postpaid":
            self.utils.click_js(xpath="//li [@data-automation-id='tab-tab-key-1']")
            if self.pageSearch_clientConsent == "agree":
                self.utils.click_js(xpath="//input[@data-automation-id='agree-customer-consent']", element_name="DA")
                self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
                self.utils.type_js(xpath="//input[@name='portedNumber']", value=prereq['Prepaid_MSISDN'])
                self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']",
                                    element_name="Trimite Token")
                self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=self.utils.retryAPI(API='Token',
                                                                                                            URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                                                            KEY=self.utils.getTempValue(
                                                                                                                'brokenMsisdn')))
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
            elif self.pageSearch_clientConsent == "disagree":
                self.utils.click_js(xpath="//input[@data-automation-id='disagree-customer-consent']", element_name="NU")
                self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
                self.utils.type_js(xpath="//input[@name='portedNumber']", value="777777777")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
        if self.pageConfigureaza_acquisitionType == "Portin prepaid":
            self.utils.click_js(xpath="//li [@data-automation-id='tab-tab-key-1']")
            if self.pageSearch_clientConsent == "agree":
                self.utils.click_js(xpath="//input[@data-automation-id='agree-customer-consent']", element_name="DA")
                self.utils.type_js(xpath="//input[@name='portedNumber']", value="777777777")
                self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']",
                                    element_name="Trimite Token")
                self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=self.utils.retryAPI(API='Token',
                                                                                                            URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                                                            KEY=self.utils.getTempValue(
                                                                                                                'brokenMsisdn')))
                self.utils.click_js(xpath="//input[@name='portedNumberType' and @value='prepaid']",
                                    element_name="Prepaid button")
                self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
                self.utils.type_js(xpath="//input[@name='invoice1']", value="50")
                self.utils.type_js(xpath="//input[@name='invoice2']", value="50")
                self.utils.type_js(xpath="//input[@name='invoice3']", value="50")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
            elif self.pageSearch_clientConsent == "disagree":
                self.utils.click_js(xpath="//input[@data-automation-id='decline-customer-consent']", element_name="NU")
                self.utils.type_js(xpath="//input[@name='portedNumber']", value=prereq['Prepaid_MSISDN'])
                self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
                self.utils.click_js(xpath="//input[@name='portedNumberType' and @value='prepaid']",
                                    element_name="Prepaid button")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
                self.utils.type_js(xpath="//input[@data-automation-id='PRENUME']", value="test")
                self.utils.type_js(xpath="//input[@data-automation-id='NUME']", value="test")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Continua")
        if self.pageConfigureaza_acquisitionType == "MFP":
            self.utils.click_js(xpath="//li [@data-automation-id='tab-tab-key-2']")
            if self.pageSearch_clientConsent == "agree":
                self.utils.click_js(xpath="//input[@data-automation-id='agree-customer-consent']", element_name="DA")
                self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']",
                                   value=prereq['Prepaid_MSISDN'])
                self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']",
                                    element_name="Trimite Token")
                self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=self.utils.retryAPI(API='Token',
                                                                                                            URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                                                            KEY=self.utils.getTempValue(
                                                                                                                'brokenMsisdn')))
                self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']",
                                   value=prereq['Prepaid_MSISDN'])
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
                self.utils.type_js(xpath="//input[@aria-activedescendant='react-select-3--value']", value="dam")
                self.utils.type_js(xpath="//input[@aria-activedescendant='react-select-4--value']", value="targoviste")
                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-5--value"]', value="aleea")
                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-6--value"]',
                                   value="Trandafirilor")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Continua")
            elif self.pageSearch_clientConsent == "disagree":
                self.utils.click_js(xpath="//input[@data-automation-id='decline-customer-consent']", element_name="NU")
                self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']",
                                   value=prereq['Prepaid_MSISDN'])
                self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']",
                                   value=prereq['Prepaid_MSISDN'])
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Urmatoarele")
                self.utils.type_js(xpath="//input[@aria-activedescendant='react-select-3--value']", value="dam")
                self.utils.type_js(xpath="//input[@aria-activedescendant='react-select-4--value']", value="targoviste")
                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-5--value"]', value="aleea")
                self.utils.type_js(xpath='//input[@aria-activedescendant="react-select-6--value"]',
                                   value="Trandafirilor")
                self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                    element_name="Continua")


class testFunctions_Dashboard:

    def __init__(self, utils):
        self.utils = utils
        self.pageDashboard_clientConsent = self.utils.get_Child(paramName='pageDashboard_clientConsent')

    def GA(self, prereq, pageLogin_prerequisite):
        self.utils.click_js(xpath='//div[@class="link-title blue " and @data-automation-id="new-subscriber-link"]',
                            element_name="Abonament Nou")
        if self.utils.element_exists(xPath='//li[@class="tab bold active " and @data-automation-id="tab-tab-key-0"]',
                                     element_name="Numar nou"):
            self.utils.log_customStep(key='Numar nou preselectat',
                                      status='PASSED')
        else:
            self.utils.log_customStep(key='Numar nou preselectat',
                                      status='FAILED')
        if self.pageDashboard_clientConsent == "agree":
            self.utils.click_js(xpath="//*[@data-automation-id='agree-customer-consent']",
                                element_name="Credit Rate DA")
            if (self.utils.element_isClickable(xpath="//button[@data-automation-id='token-consent-send-token']")):
                self.utils.log_customStep(key="Butonul consimtamant prin token este activ", status='PASSED')
            else:
                self.utils.log_customStep(key="Butonul consimtamant prin token este activ", status='FAILED')
            self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']")
            self.utils.type_js(xpath="//input[@class='validate-input ']", value=self.utils.retryAPI(API='Token',
                                                                                                    URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                                                    KEY=self.utils.getTempValue(
                                                                                                        'brokenMsisdn')))
        else:
            self.utils.click_js(xpath="//*[@data-automation-id='decline-customer-consent']",
                                element_name="Credit Rate NU")
        self.utils.type_js(xpath="//input[@name='contactNumber']", value=prereq['Prepaid_MSISDN'],
                           element_name="Numar Contact")
        if self.utils.element_isClickable(xpath="//button[@data-automation-id='subscribers-popup-continue-button']"):
            self.utils.log_customStep(key="The CONTINUA button is enabled",
                                      status='PASSED')
        else:
            self.utils.log_customStep(key="The CONTINUA button is enabled",
                                      status='FAILED')
        self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                            element_name="Continua")

        if pageLogin_prerequisite == '[Store - All Cancelled - Not Common]':
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")

    def MFP(self, prereq, pageLogin_prerequisite):
        self.utils.click_js(xpath='//div[@class="link-title blue " and @data-automation-id="new-subscriber-link"]',
                            element_name="Abonament Nou")
        self.utils.click_js(xpath="//li [@data-automation-id='tab-tab-key-2']")
        if self.pageDashboard_clientConsent == "agree":
            self.utils.click_js(xpath="//input[@data-automation-id='agree-customer-consent']", element_name="DA")
            self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']", value=prereq['Prepaid_MSISDN'])
            self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']",
                                element_name="Trimite Token")
            self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=self.utils.retryAPI(API='Token',
                                                                                                        URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                                                        KEY=self.utils.getTempValue(
                                                                                                            'brokenMsisdn')))
            self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']", value=prereq['Prepaid_MSISDN'])
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")
        elif self.pageDashboard_clientConsent == "disagree":
            self.utils.click_js(xpath='//input[@data-automation-id="decline-customer-consent"]', element_name="NU")
            self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']",
                               value=prereq['Prepaid_MSISDN'])
            self.utils.type_js(xpath="//input[@data-automation-id='numberToMigrate']", value=prereq['Prepaid_MSISDN'])
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")

        if pageLogin_prerequisite == '[Store - All Cancelled - Not Common]':
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")

    def PortinPrepaid(self, prereq, pageLogin_prerequisite):
        self.utils.click_js(xpath='//div[@class="link-title blue " and @data-automation-id="new-subscriber-link"]',
                            element_name="Abonament Nou")
        self.utils.click_js(xpath="//li [@data-automation-id='tab-tab-key-1']")
        if self.pageDashboard_clientConsent == "agree":
            self.utils.click_js(xpath="//input[@data-automation-id='agree-customer-consent']", element_name="DA")
            self.utils.type_js(xpath="//input[@name='portedNumber']", value='798654321')
            self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
            self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']",
                                element_name="Trimite Token")
            self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=self.utils.retryAPI(API='Token',
                                                                                                        URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                                                        KEY=self.utils.getTempValue(
                                                                                                            'brokenMsisdn')))
            self.utils.click_js(xpath="//input[@name='portedNumberType' and @value='prepaid']",
                                element_name="Prepaid button")
            self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
            # self.utils.type_js(xpath="//input[@name='invoice1']", value="50")
            # self.utils.type_js(xpath="//input[@name='invoice2']", value="50")
            # self.utils.type_js(xpath="//input[@name='invoice3']", value="50")
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")
        elif self.pageDashboard_clientConsent == "disagree":
            self.utils.click_js(xpath='//input[@data-automation-id="decline-customer-consent"]', element_name="NU")
            self.utils.type_js(xpath="//input[@name='portedNumber']", value='798654321')
            self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
            self.utils.click_js(xpath="//input[@name='portedNumberType' and @value='prepaid']",
                                element_name="Prepaid button")
            self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
            # self.utils.type_js(xpath="//input[@name='invoice1']", value="50")
            # self.utils.type_js(xpath="//input[@name='invoice2']", value="50")
            # self.utils.type_js(xpath="//input[@name='invoice3']", value="50")
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")

        if pageLogin_prerequisite == '[Store - All Cancelled - Not Common]':
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")

    def PortinPostpaid(self, prereq, pageLogin_prerequisite):
        self.utils.click_js(xpath='//div[@class="link-title blue " and @data-automation-id="new-subscriber-link"]',
                            element_name="Abonament Nou")
        self.utils.click_js(xpath="//li [@data-automation-id='tab-tab-key-1']")
        if self.pageDashboard_clientConsent == "agree":
            self.utils.click_js(xpath="//input[@data-automation-id='agree-customer-consent']", element_name="DA")
            self.utils.type_js(xpath="//input[@name='portedNumber']", value='798654321')
            self.utils.wait_loadingCircle(enable=True)
            self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
            self.utils.click_js(xpath="//button[@data-automation-id='token-consent-send-token']",
                                element_name="Trimite Token")
            self.utils.wait_loadingCircle(enable=True)
            self.utils.click_js(xpath="//input[@name='portedNumberType' and @value='postpaid']",
                                element_name="Postpaid button")
            self.utils.type_js(xpath="//input[@name='invoice1']", value="50")
            self.utils.type_js(xpath="//input[@name='invoice2']", value="50")
            self.utils.type_js(xpath="//input[@name='invoice3']", value="50")
            value = self.utils.retryAPI(API='Token',URL='http://devops03.connex.ro:8030/getSMSToken/',KEY=self.utils.getTempValue('brokenMsisdn'))
            print(value)
            if value == "":
                self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value= "22", tab=True)
            else:
                self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=value, tab=True)
                self.utils.click_js(xpath='//a[@data-automation-id="token-consent-validate-token"]', element_name="Valideaza")
                if self.utils.get_text(xPath='(//div[@class="error-text"]/div)[1]',enable_Logging=False) == "TOKEN incorect.":
                    self.utils.log_customStep(key="TOKEN incorect.", status="FAIL")
            time.sleep(10)
            if self.utils.element_exists(xPath="//button[@data-automation-id='subscribers-popup-continue-button']") == False:
                self.utils.log_customStep(key="Cotinua disable", status="FAIL")
            else:
                self.utils.click_js(xpath='//button[@class="primary bold right-align "]',
                                element_name="Continua")

        elif self.pageDashboard_clientConsent == "disagree":
            self.utils.click_js(xpath='//input[@data-automation-id="decline-customer-consent"]', element_name="NU")
            self.utils.type_js(xpath="//input[@name='portedNumber']", value='798654321')
            self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
            self.utils.click_js(xpath="//input[@name='portedNumberType' and @value='postpaid']",
                                element_name="Postpaid button")
            self.utils.dropDownSelector_Index(xpath="//select[@name='provider']", index="1")
            # self.utils.type_js(xpath="//input[@name='invoice1']", value="50")
            # self.utils.type_js(xpath="//input[@name='invoice2']", value="50")
            # self.utils.type_js(xpath="//input[@name='invoice3']", value="50")
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")

        if pageLogin_prerequisite == '[Store - All Cancelled - Not Common]':
            self.utils.click_js(xpath="//button[@data-automation-id='subscribers-popup-continue-button']",
                                element_name="Continua")

    def Cancelled(self):
        if self.utils.element_isClickable(xpath="//button[@data-automation-id='save-popup-save-button']"):
            self.utils.click_js(xpath="//button[@data-automation-id='save-popup-save-button']", element_name="Continua")
            self.utils.log_customStep(key='All cancelled button active',
                                      status='PASSED')
        else:
            self.utils.log_customStep(key='All cancelled button active',
                                      status='FAILED')

class TestFunctionsShipp():
    def __init__(self, utils):
        self.utils = utils
        self.pageShippingMethods_existingSIM = self.utils.get_Child(paramName="pageShippingMethods_existingSIM")

    def MainScene(self):
        get_ID_body = {
            "env": get_endPoint(endPoints.dexEnv, "PEGA_ENV"),
            "device_name": self.utils.getTempValue('device_name')
        }
        ID = self.utils.requestAPI(url='http://devops03.connex.ro:8030/getPrereq/get_device_id', body=get_ID_body)
        get_IMEI_body = {
            "'env'": get_endPoint(endPoints.dexEnv, "CM_ENV"),
            "'deviceId'": ID["Result"][0]["ITEM_CODE"]
        }
        IMEI = self.utils.requestAPI(url='http://devops03.connex.ro:8030/get_Prereq/get_IMEI', body=get_IMEI_body)
        self.utils.wait_loadingCircle()
        sim = self.utils.get_with_URL_Params(url='http://devops03.connex.ro:8030/getPrereq/get_SIM/',
                                             env="env=" + get_endPoint(endPoints.dexEnv, "CM_ENV"), key="RES")
        if self.pageShippingMethods_existingSIM == "No":
            self.utils.type_js(xpath="//input[@data-automation-id='new-sim-number-input']", value=sim, tab=True)
            self.utils.wait_loadingCircle(enable=True)
            self.utils.click_js(xpath="//button[@data-automation-id='summary-configuration-submit-btn']",
                                element_name="Continua")

        elif self.pageShippingMethods_existingSIM == "Yes":
            self.utils.click_js(xpath="//button[@data-automation-id='summary-configuration-submit-btn']",
                                element_name="Continua")
        body = {
            "env": get_endPoint(endPoints.dexEnv, "CM_ENV"),
            "sim": sim
        }
        self.utils.requestAPI(url="http://devops03.connex.ro:8030/update_Prereq/clean_sim", body=body)
        if self.utils.element_exists(xPath="//input[@data-automation-id='imei-input']"):
            self.utils.type_js(xpath="//input[@data-automation-id='imei-input']", value=IMEI["Result"]["UIN"],
                               element_name="Type IMEI")
        print(IMEI["Result"]["UIN"], "---------------------------")
        print(ID["Result"][0]["ITEM_CODE"])

    # def assignedList(self):
    #     if self.utils.get_text(xPath='(//div[@class="field-position"]//h3[@class="bold field-padding section-title"])[2]') == 'Other Settings':
    #         self.utils.log_customStep("The title of the subsection is displayed","PASSED")
    #     else:
    #         self.utils.log_customStep("The title of the subsection is not displayed", "FAIL")
    #####TBD
