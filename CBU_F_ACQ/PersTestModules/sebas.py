# from testTools import utilities as utils
import time
import os

device_name = 'Apple iPhone 7 32GB Negru'

class testFunctions_Configureaza:

    def __init__(self, utils):
        self.utils = utils
        self.offers = {
                        'Discount': 'RED Infinity 17 Test_Automation_Rate_FMC',
                        'regularOffer': 'RED 10 Test_Automation_Rate',
                        'Rate': 'RED 10 Test_Automation_Rate',
                        'Rate + Discount': 'RED Infinity 17 Test_Automation_Rate_FMC',
                        'SIM Only': 'RED Infinity 17 Test_Automation_SIM_Only',
                        'SIM Only si discount': 'RED 9 Test_Automation_SIM_Only_FMC'
                        }

        self.offers = {
                        'Discount': 'RED 9 SIM Only',
                        'regularOffer': 'RED 9 SIM Only',
                        'Rate': 'RED 9 SIM Only',
                        'Rate + Discount': 'RED 9 SIM Only',
                        'SIM Only': 'RED 9 SIM Only',
                        'SIM Only si discount': 'RED 9 SIM Only'
                        }

        self.device = {
                        'in_stock': 'Apple iPhone 7 32GB Negru',   # TAP732GBN   Huawei P Smart Auriu
                        'in_stock_testing': 'Apple iPhone 7 32GB Negru'
                        }

    def page_validations(self):

        self.utils.wait_loadingCircle(enable=True)
        header = self.utils.get_text(xPath="//div[@class='conf-header']/div[1]/div[1]/span[3]", element_name='Selected Header')

        if header == 'CONFIGUREAZĂ':
            self.utils.log_customStep(key='The Configuration screen is displayed', status='PASSED')
        else:
            self.utils.log_customStep(key='The Configuration screen is displayed', status='FAIL')

    def configCommonSteps(self):

    # CONFIGUREAZA OFERTE
        self.utils.click_js(xpath='//div[@data-automation-id="custom-configuration"]', element_name='CONFIGUREAZA OFERTE')
        time.sleep(2)
        # self.utils.wait_loadingCircle(enable=True)

    # INCEPE CU SERVICII
        incepe_servicii = self.utils.get_text(xPath='//div[@data-automation-id="header-link"]/h6/span',
                                              enable_Logging=False)
        if self.utils.element_isClickable(xpath='//div[@data-automation-id="header-link"]', enable_Logging=False) and incepe_servicii == 'ÎNCEPE CU SERVICII':
            self.utils.click_js(xpath='//div[@data-automation-id="header-link"]',
                                element_name='CONFIGUREAZA OFERTE')
        else:
            self.utils.log_customStep(key='INCEPE CU SERVICII', status='FAIL')

        if self.utils.get_text(xPath='(//div[contains(@class, "accordion_head_title")])[1]/span') == 'SERVICII MOBILE':
            self.utils.log_customStep(key='"Servicii Mobile" section is displayed first', status='PASSED')
        else:
            self.utils.log_customStep(key='"Servicii Mobile" section is displayed first', status='FAIL')

    def svOnlySteps(self):

    # DOAR SERVICII
        self.utils.wait_loadingCircle(enable=True)

        self.utils.click_js(xpath='//label[@class="switch "]')
        self.utils.wait_loadingCircle(enable=True)

        if self.utils.element_exists(xPath='//label[@class="switch "]/input[@data-automation-id="mobileDeviceOffers.mobileOfferLabel" and @value="true"]'):
            self.utils.log_customStep(key='The button is toggled on', status='PASSED')
        else:
            self.utils.log_customStep(key='The button is toggled on', status='FAIL')

        if self.utils.get_numberOfElements(xPath='(//div[contains(@class, "accordion_head_title")])') == 1 and self.utils.get_text(xPath='(//div[contains(@class, "accordion_head_title")])[1]/span') == 'SERVICII MOBILE':
            self.utils.log_customStep(key='Only the "Servicii mobile" section is displayed', status='PASSED')
        else:
            self.utils.log_customStep(key='Only the "Servicii mobile" section is displayed', status='FAIL')

        time.sleep(3)

        if not self.utils.element_exists(xPath='(//div[contains(@class, "accordion_head_title")])[2]', enable_Logging=False):
            self.utils.log_customStep(key='The "Telefoane" section is hidden', status='PASSED')
        else:
            self.utils.log_customStep(key='The "Telefoane" section is hidden', status='FAIL')

    def svOnlyViewMore(self, pageConfigureaza_offerName, pageConfigureaza_viewMore):

    # Vezi Mai Multe
    #     self.utils.log_customStep(key='CONFIG OFERTA', status='FAIL')
        if pageConfigureaza_viewMore == 'False':
            self.utils.click_js(xpath='//div[contains(@class, "accordion_head_title")]/*[text()="SERVICII MOBILE"]//ancestor::div[3]/div[2]/div[2]/div[@data-automation-id="show-more"]', element_name='Vezi Mai Multe')
            if self.utils.titleContains(title='retail/mobileOffersShowMore'):
                self.utils.log_customStep(key='More offers are displayed', status='PASSED')
            else:
                self.utils.log_customStep(key='More offers are displayed', status='FAIL')
    # Expand search Filter
            self.utils.click_js(xpath='//img[@data-automation-id="open-filter-popup"]',
                                element_name='Offer Search Filter')
    # Expand search Filter
        if pageConfigureaza_viewMore == 'True':
            self.utils.click_js(xpath='//div[contains(@class, "accordion_head_title")]/*[text()="SERVICII MOBILE"]//ancestor::div[3]//div[@data-automation-id="open-filter"]', element_name='Offer Search Filter')

    # Select Offer
        # specific offer
        # offer_name = 'test123'
        # xpath_offer_name = f'//div[@data-automation-proposal-name="{offer_name}"]/div[3]//button[@class="general_btn"]'
        # print(xpath_offer_name.replace(offer_name, self.offers[pageConfigureaza_offerName]))

        specific = 'test123'
        xpath_offer = f'//div[@data-automation-id="{specific}"]//div[3]//button[@class="general_btn"]'
            # self.utils.click_js(xpath=xpath_offer_name.replace(offer_name, self.offers['Discount']), element_name=f'Selected Offer {self.offers["Discount"]}')
            # self.utils.click_js(xpath=xpath_offer_name.replace(offer_name, self.offers[pageConfigureaza_offerName]),
            #                     element_name=f'Selected Offer {self.offers[pageConfigureaza_offerName]}')
        self.utils.type_js(xpath='//input[@data-automation-id="search-input-filter-popup"]', value=self.offers[pageConfigureaza_offerName], element_name=f'Offer {self.offers[pageConfigureaza_offerName]}')
        self.utils.click_js(xpath='//button[@data-automation-id="filter-popup-done-button"]', element_name='APLICA Offer Selection')
        self.utils.wait_loadingCircle(enable=True)

        # 1st offer
        self.utils.click_js(xpath=xpath_offer.replace(specific, '0'))

            # self.utils.click_js(xpath=xpath_offer_name.replace(offer_name, self.offers[pageConfigureaza_offerName]),
            #                     element_name=f'Selected Offer {self.offers[pageConfigureaza_offerName]}')

        self.utils.wait_loadingCircle(enable=True)

    def viewMoreSteps(self, pageConfigureaza_offerName):

    # Vezi Mai Multe
        self.utils.click_js(xpath='//div[@data-automation-id="show-more"]', element_name='Vezi Mai Multe')
        self.utils.wait_loadingCircle(enable=True)
        if self.utils.titleContains(title='retail/mobileOffersShowMore'):
            self.utils.log_customStep(key='More offers are displayed', status='PASSED')
        else:
            self.utils.log_customStep(key='More offers are displayed', status='FAIL')

        self.utils.scroll(nr_scrolls=5)

    # Expand search Filter
        self.utils.click_js(xpath='//img[@data-automation-id="open-filter-popup"]', element_name='Offer and Device Search Filter')
        time.sleep(3)

    # Input Device and offer
        self.utils.type_js(xpath='//input[@name="searchDeviceInput"]', value=self.device['in_stock'], element_name='Device')
        self.utils.type_js(xpath='//input[@name="searchInput"]', value=self.offers[pageConfigureaza_offerName],
                           element_name='Offer')

        if self.utils.element_isClickable(xpath='//button[@data-automation-id="filter-popup-done-button"]',
                                          enable_Logging=False):
            self.utils.click_js(xpath='//button[@data-automation-id="filter-popup-done-button"]', element_name='APLICA')

    # Select retrieved offer
        if self.utils.element_exists(xPath='//div[@data-automation-id="0"]//div[3]//button[@class="general_btn"]', enable_Logging=False):
            self.utils.click_js(xpath='//div[@data-automation-id="0"]//div[3]//button[@class="general_btn"]', element_name='Selecteaza')
        else:
            self.utils.log_customStep(key='Bundle Offers Retrieved', status='FAIL')

    def selectOffer(self, pageConfigureaza_offerName):

        if pageConfigureaza_offerName == 'Rate':
            pass
        elif pageConfigureaza_offerName == 'Rate + Discount':
            pass
        elif pageConfigureaza_offerName == 'SIM Only':
            pass
        elif pageConfigureaza_offerName == 'SIM Only si Discount':
            pass

    def bundleSteps(self, pageConfigureaza_offerName, search_specific=False):

        if search_specific:
    # Expand Hamburger Search Filter
            self.utils.click_js(xpath='//div[@data-automation-id="open-filter"]', element_name='Search Filter')
            self.utils.wait_loadingCircle(enable=True)

    # Input Device and offer
            self.utils.type_js(xpath='//input[@name="searchDeviceInput"]', value=self.device['in_stock'], element_name='Device')
            self.utils.type_js(xpath='//input[@name="searchInput"]', value=self.offers[pageConfigureaza_offerName], element_name='Offer')

            if self.utils.element_isClickable(xpath='//button[@data-automation-id="filter-popup-done-button"]', enable_Logging=False):
                self.utils.click_js(xpath='//button[@data-automation-id="filter-popup-done-button"]', element_name='APLICA')

    # Select retrieved offer
        if self.utils.element_exists(xPath='//div[@data-automation-id="0"]//div[3]//button[@class="general_btn"]', enable_Logging=False):
            self.utils.click_js(xpath='//div[@data-automation-id="0"]//div[3]//button[@class="general_btn"]', element_name='Selecteaza')
        else:
            self.utils.log_customStep(key='Bundle Offers Retrieved', status='FAIL')

    def DevtoServ(self):

    # Incepe cu Telefon
        self.utils.click_js(xpath='//div[@data-automation-id="header-link"]', element_name='INCEPE CU TELEFON')
        self.utils.wait_loadingCircle(enable=True)

        if self.utils.get_text(xPath='(//div[contains(@class, "accordion_head_title")])[1]/span', enable_Logging=False) == 'TELEFOANE':
            self.utils.log_customStep(key='TELEFOANE Section Displayed First', status='PASSED')
        else:
            self.utils.log_customStep(key='TELEFOANE Section Displayed First', status='FAIL')

    def DevtoServViewMore(self, pageConfigureaza_viewMore):

        if pageConfigureaza_viewMore == 'False':
    # Vezi Mai Multe
            self.utils.click_js(xpath='//div[contains(@class, "accordion_head_title")]/*[text()="TELEFOANE"]//ancestor::div[3]/div[2]/div[2]/div[@data-automation-id="show-more"]', element_name='Vezi Mai Multe')
            self.utils.wait_loadingCircle(enable=True)
            self.utils.titleContains(title='retail/devices')

    # Expand Hamburger Search Filter
            self.utils.click_js(xpath='//img[@data-automation-id="open-filter-popup"]', element_name='Search Filter')
            self.utils.wait_loadingCircle(enable=True)

        elif pageConfigureaza_viewMore == 'True':
            self.utils.click_js(xpath='//div[contains(@class, "accordion_head_title")]/*[text()="TELEFOANE"]//ancestor::div[3]//div[@data-automation-id="open-filter"]', element_name='Search Filter')
            time.sleep(3)

        self.utils.type_js(xpath='//input[@name="searchDeviceInput"]', value=self.device['in_stock_testing'])
        self.utils.click_js(xpath='//button[@data-automation-id="filter-popup-done-button"]', element_name='APLICA')
        self.utils.wait_loadingCircle(enable=True)

        print(self.utils.element_exists(xPath='(//div[contains(@class, "card_wrap card-wrap-device")])[1]//button[contains(@class, "general_btn")]'))
    # Select First Device
        if self.utils.element_exists(xPath='(//div[contains(@class, "card_wrap card-wrap-device")])[1]//button[contains(@class, "general_btn")]', enable_Logging=False):
            time.sleep(5)
            print(f'//div[contains(@title,"{self.device["in_stock_testing"]}")]//ancestor::div[3]//button[contains(@class, "general_btn")]')
            self.utils.click_js(xpath=f'//div[contains(@title,"{self.device["in_stock_testing"]}")]//ancestor::div[3]//button[contains(@class, "general_btn")]/span', element_name='SELECTEAZA')
            self.utils.wait_loadingCircle(enable=True)
        else:
            self.utils.log_customStep(key='Device found', status='FAIL')

    def checkContinueStatus(self, enabled=True):

        if enabled:
            if self.utils.is_element_Enabled(
                    xPath='//button[@data-automation-id="summary-configuration-submit-btn"]', enable_Logging=False):
                self.utils.log_customStep(key='The "Continua" button is enabled',
                                          status='PASSED')
            else:
                self.utils.log_customStep(key='The "Continua" button is enabled', status='FAIL')
        else:
            if not self.utils.is_element_Enabled(xPath='//button[@data-automation-id="summary-configuration-submit-btn"]', enable_Logging=False):
                self.utils.log_customStep(key='The "Continua" button is disabled and cannot be clicked', status='PASSED')
            else:
                self.utils.log_customStep(key='The "Continua" button is disabled and cannot be clicked', status='FAIL')

    def stockBehaviour(self):
        pass

    def RegularFlow(self):
        pass

    def ServtoDev(self):
        pass


class testFunctions_uploadDocuments():

    def __init__(self, utils):
        self.utils = utils

    def page_checks(self):

        if self.utils.titleContains(title='retail/uploadDocuments'):
            self.utils.log_customStep(key='Page Upload Documents loaded successfully', status='PASSED')
        else:
            self.utils.log_customStep(key='Page Upload Documents loaded successfully', status='FAIL')

        if not self.utils.is_element_Enabled(xPath='//button[@data-automation-id="summary-configuration-submit-btn"]', enable_Logging=False):
            self.utils.log_customStep(key='FINALIZARE Btn Disabled', status='PASSED')
        else:
            self.utils.log_customStep(key='FINALIZARE Btn Disabled', status='FAIL')

    def docSendFirstOp(self):

        self.utils.click_js(xpath='(//input[@name="documentTransmissionOption"])[1]', element_name='First Option')
        doc_message = self.utils.get_text(xPath='(//input[@name="documentTransmissionOption"])[1]//ancestor::div[2]/div[2]/span')

        if 'www.vodafone.ro' in doc_message.lower() and 'iunea legal.' in doc_message.lower():
            self.utils.log_customStep(key='Message for First Option', status="PASSED")
        else:
            self.utils.log_customStep(key='Message for First Option', status="FAIL")

    def docSendSecondOp(self):

        self.utils.click_js(xpath='(//input[@name="documentTransmissionOption"])[2]', element_name='Second Option')
        doc_message = self.utils.get_text(
            xPath='(//input[@name="documentTransmissionOption"])[2]//ancestor::div[2]/div[2]/span')

        if 'www.vodafone.ro' not in doc_message.lower() and 'iunea legal.' not in doc_message.lower():
            self.utils.log_customStep(key='Message for Second Option', status="PASSED")
        else:
            self.utils.log_customStep(key='Message for Second Option', status="FAIL")

    def paperSignature(self):

    # Check toggle is set to Electronic
        if self.utils.element_exists(xPath='//input[@data-automation-id="electronic-signing-toggle" and @value="true"]'):
            self.utils.log_customStep(key='Semnatura Electronica Selected', status="PASSED")
        else:
            self.utils.log_customStep(key='Semnatura Electronica Selected', status="FAIL")

        self.utils.click_js(xpath='//input[@data-automation-id="electronic-signing-toggle"]//ancestor::label', element_name='Semnatura Electronica OFF')
        final_togle = self.utils.element_exists(xPath='//input[@data-automation-id="electronic-signing-toggle" and @value="false"]')
        # print(final_togle, 'final_togle')

        if not self.utils.element_exists(xPath='(//div[@class="wrap-bundle-item"])[3]', enable_Logging=False) and final_togle:
            self.utils.log_customStep(key='The "Electronic signing" subsection is closed and the toggle is set to OFF', status="PASSED")
        else:
            self.utils.log_customStep(key='The "Electronic signing" subsection is closed and the toggle is set to OFF', status="FAIL")

    def electronicSignature(self, pageUploadDocuments_electronicMethod):

    # Check toggle is set to Electronic
        if self.utils.element_exists(xPath='//input[@data-automation-id="electronic-signing-toggle" and @value="true"]'):
            self.utils.log_customStep(key='Semnatura Electronica Selected', status="PASSED")
        else:
            self.utils.log_customStep(key='Semnatura Electronica Selected', status="FAIL")

        if pageUploadDocuments_electronicMethod == 'Digital':

            self.utils.click_js(xpath='(//div[@class="wrap-bundle-item"])[3]/div[1]/input', element_name='Digital')

    # SMS and Email checks
            text_1 = self.utils.get_text(xPath='(//div[@class="wrap-bundle-item"])[3]/div[2]')
            if 'sms' in text_1.split(';')[0].lower() and 'către:' in text_1.split(';')[0].lower():
                self.utils.log_customStep(key='SMS Catre', status="PASSED")
            else:
                self.utils.log_customStep(key='SMS Catre', status="FAIL")

            if 'email' in text_1.split(';')[1].lower() and 'către:' in text_1.split(';')[1].lower():
                self.utils.log_customStep(key='Email Catre', status="PASSED")
            else:
                self.utils.log_customStep(key='Email Catre', status="FAIL")

            if len(text_1.split(';')[0].split(':')[1].strip()) == 9 and text_1.split(';')[0].split(':')[
                1].strip().startswith('7'):
                self.utils.log_customStep(key='Phone Number', status="PASSED")
            else:
                self.utils.log_customStep(key='Phone Number', status="FAIL")

            if '@' in text_1.split(';')[1].split(':')[1].strip() and '.' in text_1.split(';')[1].split(':')[1].strip():
                self.utils.log_customStep(key='Email value', status="PASSED")
            else:
                self.utils.log_customStep(key='Email value', status="FAIL")

        elif pageUploadDocuments_electronicMethod == 'Digital Paper':
            self.utils.click_js(xpath='(//div[@class="wrap-bundle-item"])[4]/div[1]/input', element_name='Digital Hartie')

    def docSteps(self, pageUploadDocuments_docOpt, clickIncarcaDoc):

        self.utils.click_js(xpath='//button[contains(@class, "generate-document")]', element_name='GENEREAZA DOCUMENTE')
        self.utils.wait_loadingCircle(enable=True)
        # time.sleep(3)

    # Mobile Contract Static
        text1 = self.utils.get_text(xPath='//div[@class="contracts-section"]/div[1]/span')
        status1 = self.utils.get_text(xPath='//div[@class="contracts-section"]/div[1]/span[2]/span')

        if 'mobile contract static' in text1.lower() and 'generat' in status1.lower():
            self.utils.log_customStep(key='The "Mobile Contract Static" document is generated', status='PASSED')
        else:
            self.utils.log_customStep(key='The "Mobile Contract Static" document is generated', status='FAIL')

        self.utils.click_js(xpath='(//span[@data-automation-id="generate-contract"])[1]', element_name='VIZUALIZARE Mobile Contract Static')
        time.sleep(5)
        if self.utils.get_numberOfTabs() == 2:
            self.utils.log_customStep(key='A new tab is opened where the user can view the document in a PDF format', status='PASSED')
        else:
            self.utils.log_customStep(key='A new tab is opened where the user can view the document in a PDF format', status='FAIL')

    # Mobile Contract CBU
        text1 = self.utils.get_text(xPath='//div[@class="contracts-section"]/div[2]/span')
        status1 = self.utils.get_text(xPath='//div[@class="contracts-section"]/div[2]/span[2]/span')

        if 'mobile contract cbu' in text1.lower() and 'generat' in status1.lower():
            self.utils.log_customStep(key='The "Mobile Contract CBU" document is generated', status='PASSED')
        else:
            self.utils.log_customStep(key='The "Mobile Contract CBU" document is generated', status='FAIL')

        self.utils.click_js(xpath='(//span[@data-automation-id="generate-contract"])[2]',
                            element_name='VIZUALIZARE Mobile Contract CBU')
        time.sleep(5)
        if self.utils.get_numberOfTabs() == 3:
            self.utils.log_customStep(key='A new tab is opened where the user can view the document in a PDF format',
                                      status='PASSED')
        else:
            self.utils.log_customStep(key='A new tab is opened where the user can view the document in a PDF format',
                                      status='FAIL')

    # Contract TCG Static
        if pageUploadDocuments_docOpt == 'secondOption':
            text1 = self.utils.get_text(xPath='//div[@class="contracts-section"]/div[3]/span')
            status1 = self.utils.get_text(xPath='//div[@class="contracts-section"]/div[3]/span[2]/span')

            if 'contract tcg static' in text1.lower() and 'generat' in status1.lower():
                self.utils.log_customStep(key='The "Contract TCG Static" document is generated', status='PASSED')
            else:
                self.utils.log_customStep(key='The "Contract TCG Static" document is generated', status='FAIL')

            self.utils.click_js(xpath='(//span[@data-automation-id="generate-contract"])[3]',
                                element_name='VIZUALIZARE Contract TCG Static')
            time.sleep(5)
            if self.utils.get_numberOfTabs() == 4:
                self.utils.log_customStep(
                    key='A new tab is opened where the user can view the document in a PDF format',
                    status='PASSED')
            else:
                self.utils.log_customStep(
                    key='A new tab is opened where the user can view the document in a PDF format',
                    status='FAIL')

    # Return to uploadDocuments TAB
        self.utils.moveToNewTab(index=0, tabName='uploadDocuments')
        time.sleep(15)

    # INCARCA DOC Disabled
        if not self.utils.is_element_Enabled(xPath='//button[@class="upload-document"]'):
            self.utils.log_customStep(key='INCARCA DOCUMENTE Disabled', status='PASSED')
        else:
            self.utils.log_customStep(key='INCARCA DOCUMENTE Disabled', status='FAIL')

    # Upload Doc
        os.environ['PROJECT_ROOT'] = os.path.dirname(os.path.abspath(__file__))
        filepath = str(os.environ['PROJECT_ROOT']) + '\\Resources\\Doc_for_DEXRegr_uploadDoc.pdf'
        # print(filepath)
        # import pdb;pdb.set_trace()
        self.utils.upload_document(xpath='//input[@data-automation-id="drop-zone"]', filePath=filepath,
                                   element_name='UPLOAD Contract')
        self.utils.wait_loadingCircle(enable=True)
        if self.utils.element_exists(xPath='//div[@class="file-zone-container"]', element_name='Documents Uploaded'):
            self.utils.log_customStep(key='The documents are uploaded into the system', status='PASSED')
        else:
            self.utils.log_customStep(key='The documents are uploaded into the system', status='FAIL')

    # INCARCA DOC Enabled
        if self.utils.is_element_Enabled(xPath='//button[@class="upload-document"]'):
            self.utils.log_customStep(key='INCARCA DOCUMENTE Enabled', status='PASSED')
        else:
            self.utils.log_customStep(key='INCARCA DOCUMENTE Enabled', status='FAIL')

    # WA for loading error page after clicking "INCARCA DOCUMENTE"
        if clickIncarcaDoc == 'Yes':
            time.sleep(2)
            self.utils.click_js(xpath='//button[@class="upload-document"]', element_name='INCARCA DOCUMENTE')
            self.utils.wait_loadingCircle(enable=True)

        self.utils.click_js(xpath='//button[@class="submit-order"]', element_name='INREGISTREAZA COMANDA')
        self.utils.wait_loadingCircle(enable=True)

        if self.utils.is_element_Enabled(xPath='//button[@data-automation-id="summary-configuration-submit-btn"]'):
            self.utils.log_customStep(key='FINALIZARE Enabled', status='PASSED')
        else:
            self.utils.log_customStep(key='FINALIZARE Enabled', status='FAIL')

        self.utils.click_js(xpath='//button[@data-automation-id="summary-configuration-submit-btn"]', element_name='FINALIZARE')
        self.utils.wait_loadingCircle(enable=True)

        if self.utils.element_exists(xPath='//div[@class="warning-section"]', element_name='Warning Message'):
            self.utils.click_js(xpath='//button[@data-automation-id="primary-button-in-popup"]', element_name='Continua From Warning')
            self.utils.log_customStep(key='Documentele nu au fost incarcate', status='FAIL')
            self.utils.wait_loadingCircle(enable=True)

    # FINAL Pop Up
        if self.utils.element_exists(xPath='//img[@class="x"]', enable_Logging=False):
            self.utils.log_customStep(key='Final POP Up Opened', status='PASSED')
        else:
            self.utils.log_customStep(key='Final POP Up Opened', status='FAIL')

    # Client DASHBOARD
        dash_client = self.utils.get_text(xPath='//button[@data-automation-id="Customer-Dashboard-btn"]/span')
        # print(dash_client,'dash client________________')
        # print(self.utils.element_isClickable(xpath='//button[@data-automation-id="Customer-Dashboard-btn"]/span'), 'client cu span')
        # print(self.utils.element_isClickable(xpath='//button[@data-automation-id="Customer-Dashboard-btn"]'),
        #   'client FARA span')
        if self.utils.element_isClickable(xpath='//button[@data-automation-id="Customer-Dashboard-btn"]/span') and 'dashboard client' in dash_client.lower():
            self.utils.log_customStep(key='The user is able to view and access link Client Dashboard', status='PASSED')
        else:
            self.utils.log_customStep(key='The user is able to view and access link Client Dashboard', status='FAIL')

    # Agent DASHBOARD
        dash_agent = self.utils.get_text(xPath='//button[@data-automation-id="Agent-Dashboard-btn"]/span')
        # print(dash_agent,'dash agent________________')
        # print(self.utils.element_isClickable(xpath='//button[@data-automation-id="Agent-Dashboard-btn"]/span'), 'agent cu span')
        # print(self.utils.element_isClickable(xpath='//button[@data-automation-id="Agent-Dashboard-btn"]'),
        #   'agent FARA span')
        if self.utils.element_isClickable(xpath='//button[@data-automation-id="Agent-Dashboard-btn"]/span') and 'dashboard agent' in dash_agent.lower():
            self.utils.log_customStep(key='The user is able to view and access link Agent Dashboard', status='PASSED')
        else:
            self.utils.log_customStep(key='The user is able to view and access link Agent Dashboard', status='FAIL')

    # Order Details
        order_id = self.utils.get_text(xPath='//span[@class="order-success"]/span')
        if self.utils.element_isClickable(xpath='//span[@class="order-success"]/span') and 'order #' in order_id.lower():
            self.utils.log_customStep(key='The user is able to view and access link Order Details', status='PASSED')
        else:
            self.utils.log_customStep(key='The user is able to view and access link Order Details', status='FAIL')


class testFunctions_OrderSummary():

    def __init__(self, utils):
        self.utils = utils

    def page_summary_checks(self, pageConfigureaza_offerType):

        if self.utils.titleContains(title='retail/orderSummary'):
            self.utils.log_customStep(key='Page Order Summary loaded successfully', status='PASSED')
        else:
            self.utils.log_customStep(key='Page Order Summary loaded successfully', status='FAIL')

    # TOTAL DE PLATA
        header_name = self.utils.get_text(xPath='//div[@class="order-summary-bundlse"]/div/div/div/span', enable_Logging=False)
        if 'total de plat' in header_name.lower():
            self.utils.log_customStep(key='"TOTAL DE PLATA" Section displayed', status='PASSED')
        else:
            self.utils.log_customStep(key='"TOTAL DE PLATA" Section displayed', status='FAIL')

    # LUNAR
        header_name = self.utils.get_text(xPath='(//div[@class="right-wrap"])[1]//div[@class="sub-title"]//*[contains(text(), "LUNAR")]',
                                          enable_Logging=False)
        if 'lunar' in header_name.lower():
            self.utils.log_customStep(key='"LUNAR" Section displayed', status='PASSED')
        else:
            self.utils.log_customStep(key='"LUNAR" Section displayed', status='FAIL')

        # Lunar Value
        total_plata = self.utils.get_text(xPath='(//div[@class="right-wrap"])[1]//div[@class="sub-title"]//*[contains(text(), "LUNAR")]//ancestor::div[1]//ancestor::div[1]/div[2]', enable_Logging=False)
        print(total_plata)
        # self.utils.setTempValue(key='sumalunar', value='10,19')
        total = self.utils.getTempValue('sumalunar')
        print(total, 'total lunar')
        if '€' in total_plata and str(total_plata.split('€')[0]) == str(total):
            self.utils.log_customStep(key='"Lunar" displays the correct value', status='PASSED')
        else:
            self.utils.log_customStep(key='"Lunar" displays the correct value', status='FAIL')

        # Total Avans
        # self.utils.setTempValue(key='sumaAvans', value='10.19')
        totalAvans = self.utils.getTempValue('sumaAvans')
        if pageConfigureaza_offerType == 'serviceAndDevice':
            if totalAvans == '':
                pass

    # 3 Dots
        if self.utils.element_exists(xPath='//button[@data-automation-id="apply-personal-details-to-contact"]', enable_Logging=False):
            self.utils.log_customStep(key='3 dots menu button is displayed', status='PASSED')
        else:
            self.utils.log_customStep(key='3 dots menu button is displayed', status='FAIL')

    def dotsActions(self):

        self.utils.click_js(xpath='//button[@data-automation-id="apply-personal-details-to-contact"]', element_name='3 Dots Button - The menu is displayed')
        time.sleep(1)

    # Vezi document
        if self.utils.element_exists(xPath='//div[@class="dropdown-items border-bottom "]/span', enable_Logging=False):
            if self.utils.get_text(xPath='//div[@class="dropdown-items border-bottom "]/span', enable_Logging=False).lower() == 'vezi document':
                self.utils.click_js(xpath='//div[@class="dropdown-items border-bottom "]/span', element_name='Vezi document')
                self.utils.wait_loadingCircle(enable=True)
                if self.utils.get_numberOfTabs() == 2:
                    self.utils.log_customStep(key='A new tab is opened displaying the summary in PDF format', status='PASSED')
                else:
                    self.utils.log_customStep(key='A new tab is opened displaying the summary in PDF format', status='FAIL')
                self.utils.moveToNewTab(index=0, tabName='OrderSummary')
                time.sleep(5)

    # Trimite pe Email
    #     self.utils.click_js(xpath='//button[@data-automation-id="apply-personal-details-to-contact"]', element_name='3 Dots Button - The menu is displayed')
        # time.sleep(1)

        if self.utils.get_text(xPath='//div[@class="agent-dropdown open container"]/div[2]/span', enable_Logging=False).lower() == 'trimite pe email':
            self.utils.click_js(xpath='//div[@class="agent-dropdown open container"]/div[2]/span', element_name='Trimite pe email')
            self.utils.wait_loadingCircle(enable=True)
            if self.utils.element_exists(xPath='//div[@class="wrap-popup "]', enable_Logging=False):
                self.utils.log_customStep(key='A pop-up is displayed to fill in the first name, name and email information', status='PASSED')
            else:
                self.utils.log_customStep(key='A pop-up is displayed to fill in the first name, name and email information', status='FAIL')
        else:
            self.utils.log_customStep(key='Trimite pe Email', status='FAIL')

        self.utils.type_js(xpath='//input[@name="personFirstName"]', value='FirstName')
        self.utils.type_js(xpath='//input[@name="personLastName"]', value='LastName')
        self.utils.type_js(xpath='//input[@name="contactEmail"]', value='test@test.com')

    def showDocFromPopUp(self):

        self.utils.click_js(xpath='//div[@class="link-title blue uppercase inline-flex no-arrow"]/h4', element_name='VIZUALIZEAZA')
        time.sleep(3)
        if self.utils.get_numberOfTabs() == 3 and self.utils.get_text(xPath='//div[@class="link-title blue uppercase inline-flex no-arrow"]/h4') == 'VIZUALIZEAZĂ':
            self.utils.log_customStep(key='A new tab displaying the summary in PDF format is opened', status='PASSED')
        else:
            self.utils.log_customStep(key='A new tab displaying the summary in PDF format is opened', status='FAIL')

        self.utils.moveToNewTab(index=0, tabName='OrderSummary')
        time.sleep(2)

    def sendEmail(self):

    # TRIMITE
        self.utils.click_js(xpath='//button[@class="confirmationButton primary bold"]', element_name='TRIMITE')
        # self.utils.wait_loadingCircle(enable=True)
    # Window with Email details is closed and success pop displayed

        if self.utils.element_exists(xPath='//div[@class="toaster-content"]/i', enable_Logging=False): # not self.utils.element_exists(xPath='//div[@class="agent-dropdown open container"]/div[2]/span', enable_Logging=False) and
            self.utils.log_customStep(key='A notification pop-up is generated, informing the user that the email was successfully sent or not',
                                      status='PASSED')
        else:
            self.utils.log_customStep(key='A notification pop-up is generated, informing the user that the email was successfully sent or not', status='FAIL')

        if not self.utils.titleContains(title='orderSummary'):
            self.utils.log_customStep(
                key='The user is returned to the previous screen',
                status='FAIL')

    def summaryChecks(self):

    # offer Name from shoppingCart
    #     self.utils.setTempValue(key='serviceName', value='RED 9 Credit Rate 0')
        offer_name_shopcrt = self.utils.getTempValue('serviceName')
        offer_name_sum = self.utils.get_text(xPath='//div[@class="top-item-bundle bold"]/*[contains(text(), "OFERT") or contains(text(), "SERVICII")]//ancestor::div[2]//div[contains(@class, "bundle-name mobile-bundle-header")]', enable_Logging=False)
        offer_name_final = str(offer_name_sum.split('Credit Rate')[0]) + 'Credit Rate ' + str(offer_name_sum.split('Credit Rate')[1].strip()[0])

        if offer_name_final == offer_name_shopcrt:
            self.utils.log_customStep(key='"Oferta servicii" displays the offer that the user has selected in the Configuration page', status='PASSED')
        else:
            self.utils.log_customStep(key='"Oferta servicii" displays the offer that the user has selected in the Configuration page', status='FAIL')

    def GDPRActions(self, pageOrderSummary_GDPR):

        gdpr_title = self.utils.get_text(xPath='//div[@class="order-summary-general-data"]/div/h2/span')

        # Title section
        if gdpr_title == 'PROTECTIA DATELOR CU CARACTER PERSONAL':
            com_comercial = self.utils.get_text(xPath='(//div[@class="line-layout first-line"])[1]/div/h2/span')

            # COMUNICARI COMERCIALE SI PROFILARE UTILIZATOR - Title subsection and collapsed menu
            if com_comercial == 'COMUNICARI COMERCIALE SI PROFILARE UTILIZATOR' and 'arrow' not in self.utils.get_attrValue(xPath='(//div[@class="line-layout first-line"])[1]/div[1]', attrName='class').lower():

                # Check VDF 1 YES All & Check Partner 1 YES All
                time.sleep(3)
                if self.utils.element_exists(xPath='//input[@data-automation-id="automation-vf-basic" and @value="true"]') and self.utils.element_exists(xPath='//input[@data-automation-id="automation-partner-basic" and @value="true"]'):
                    self.utils.log_customStep(key='The "Comunicari comerciale si profilare utilizator" subsection is by default closed with Vodafone and Partner options toggled ON to "Da toate"', status='PASSED')
                else:
                    self.utils.log_customStep(
                        key='The "Comunicari comerciale si profilare utilizator" subsection is by default closed with Vodafone and Partner options toggled ON to "Da toate"',
                        status='FAIL')

            # PROFILARE AVANSATA DATE DE TRAFIC, LOCALIZARE SI INTERNET - Title subsection and expanded menu
            profil_av = self.utils.get_text(xPath='(//div[@class="line-layout first-line"])[2]/div/h2/span')

            if profil_av == 'PROFILARE AVANSATA DATE DE TRAFIC, LOCALIZARE SI INTERNET' and 'arrow' in self.utils.get_attrValue(
                    xPath='(//div[@class="line-layout first-line"])[2]/div[1]', attrName='class').lower():

                # Check VDF 2 No All & Check Partner 2 No All

                if self.utils.element_exists(xPath='//input[@data-automation-id="automation-vf-advanceProfiling" and @value="false"]', enable_Logging=False) and self.utils.element_exists(xPath='//input[@data-automation-id="automation-partner-advanceProfiling" and @value="false"]', enable_Logging=False):
                    self.utils.log_customStep(
                        key='The "Profilare avansata date de trafic, localizare si internet" subsection is by default expanded with Vodafone and Partner options toggled OFF to "Da toate"',
                        status='PASSED')
                else:
                    self.utils.log_customStep(
                        key='The "Profilare avansata date de trafic, localizare si internet" subsection is by default expanded with Vodafone and Partner options toggled OFF to "Da toate"',
                        status='FAIL')

        # CONTINUA Disabled
        if self.utils.is_element_Enabled(xPath='//button[@data-automation-id="summary-configuration-submit-btn"]'):
            self.utils.log_customStep(key='CONTINUA Button Disabled', status='FAIL')

        # Expand COMUNICARI COMERCIALE
        self.utils.click_js(xpath='(//div[@class="line-layout first-line"])[1]/div[1]/h2',
                            element_name='Expand COMUNICARI COMERCIALE')

        time.sleep(5)

        if pageOrderSummary_GDPR == 'noALL':

            # NO Vdf
            for i in range(1, 6):
                self.utils.click_js(xpath='(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i)
                                          + ']//input)[2]')
            # NO Partner
            for i in range(1, 6):
                self.utils.click_js(xpath='(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i)
                                          + ']//input)[4]')

        elif pageOrderSummary_GDPR == 'noVDF_yesPartners':

            # NO VDF
            # self.utils.click_js(xpath='//input[@data-automation-id="automation-vf-basic"]')
            for i in range(1, 6):
                self.utils.click_js(xpath='(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i)
                                          + ']//input)[2]')

            # Yes Partner
            time.sleep(3)
            self.utils.click_js(xpath='//input[@data-automation-id="automation-partner-advanceProfiling"]//ancestor::label')

        elif pageOrderSummary_GDPR == 'yesALL':

            # Yes VDF
            self.utils.click_js(xpath='//input[@data-automation-id="automation-vf-advanceProfiling"]//ancestor::label')

            # Yes Partner
            self.utils.click_js(xpath='//input[@data-automation-id="automation-partner-advanceProfiling"]//ancestor::label')

        elif pageOrderSummary_GDPR == 'yesVDF_noPartners':

            # Yes VDF
            self.utils.click_js(
                xpath='//input[@data-automation-id="automation-vf-advanceProfiling"]//ancestor::label')

            # NO Partner
            for i in range(1, 6):
                self.utils.click_js(xpath='(//div[@class="order-summary-general-data"]/div[2]/div[2]/div[' + str(i)
                                          + ']//input)[4]')

    def sendOptions(self):

    # CONTINUA Enabled
        if not self.utils.element_isClickable(xpath='//button[@data-automation-id="summary-configuration-submit-btn"]'):
            self.utils.log_customStep(key='CONTINUA Button Enabled', status='FAIL')

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Final Click on "CONTINUA" _ Disabled for testing
        self.utils.click_js(xpath='//button[@data-automation-id="summary-configuration-submit-btn"]', element_name='CONTINUA')

        self.utils.wait_loadingCircle(enable=True)