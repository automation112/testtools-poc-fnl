from datetime import date
import random
import time


class testFunctions_Dashboard:

    def __init__(self, utils):
        self.utils = utils

    def detaliiClient(self):

        if self.utils.element_exists(xPath='//div[@class="customer-details"]//span[contains(text(),"DETALII CLIENT")]', element_name='Detalii client', enable_Logging=False) == True:

            nume = self.utils.get_text(xPath='(//div[@class="customer-details"]//div[@class="item-data"]/div[@class="value"])[1]', element_name='Nume Client', enable_Logging=False)
            email = self.utils.get_text(xPath='(//div[@class="customer-details"]//div[@class="item-data"]/div[@class="value"])[2]', element_name='Email Client', enable_Logging=False)
            telefon = self.utils.get_text(xPath='(//div[@class="customer-details"]//div[@class="item-data"]/div[@class="value"])[3]', element_name='Telefon Client', enable_Logging=False)
            telefon_alternativ = self.utils.get_text(xPath='(//div[@class="customer-details"]//div[@class="item-data"]/div[@class="value"])[4]', element_name='Telefon Alternativ Client', enable_Logging=False)
            adresa = self.utils.get_text(xPath='(//div[@class="customer-details"]//div[@class="item-data"]/div[@class="value"])[5]', element_name='Adresa Client', enable_Logging=False)

            self.utils.setTempValue(key="numeDashboard", value=nume)
            self.utils.setTempValue(key="emailDashboard", value=email)
            self.utils.setTempValue(key="telefonDashboard", value=telefon)
            self.utils.setTempValue(key="telefon_alternativDashboard", value=telefon_alternativ)
            self.utils.setTempValue(key="adresaDashboard", value=adresa)
        else:
            self.utils.log_customStep(key='!FAIL! Detalii Client Field (From Dashboar) does not exists',status= 'Fail')






class testFunctions_Configureaza:

    def __init__(self, utils):
        self.utils = utils


    def editAddOn(self):
        self.utils.log_customStep(key='!FAIL! This scenario(editAddOn) is unavailable - move to noChange scenario',status= 'Fail')
        self.noChange()

    def changeSelection(self):
        self.utils.click_js(xpath='(//div[@class="accordion  open-accordion  "])[1]//div[@class="addon_item "]',
                            element_name="Oferta - Optiuni Promotionale")
        self.utils.click_js(xpath='(//div[@class="accordion  open-accordion  "])[2]//div[@class="addon_item "]',
                            element_name="Oferta - Optiuni Standard")
        self.utils.click_js(
            xpath='//div[@class="configuration-addon-footer"]/button[@class="primary bold configuraton-pop-up-btn "]',
            element_name="Selecteaza - Configure Add-Ons Pop-up")
        self.utils.click_js(xpath='//div[@class="footer-link margin-right-30 "]', element_name="Valideaza")
        self.utils.wait_loadingCircle(enable=True)
        if self.utils.element_exists(xPath='//div[@class="title-popup flex-start title-height"]//h3',
                                     enable_Logging=False):
            self.utils.log_customStep(key='!FAIL! - EPC Error | Addons Error', status='Fail')
            self.utils.click_js(xpath='//button[@class="confirmationButton primary bold"]',
                                element_name='Inchide Pop-up')
            self.utils.click_js(xpath='//div[@class="addOns"]//div[@class="accordion_head_title  "]/span[contains(text(),"STANDAR")]//ancestor::div[5]//div[@class="addon_item selected"]',
                                element_name="Oferta - Optiuni Standard", enable_Logging=False)
            self.utils.click_js(xpath='//div[@class="footer-link margin-right-30 "]', element_name="Valideaza", enable_Logging=False)
            self.utils.wait_loadingCircle(enable=True)
            self.addonsCommon()
        else:
            self.addonsCommon()

    def noChange(self):
        self.addonsCommon()


    def addonsCommon(self):
        self.utils.click_js(xpath='//img[@src="resources/assets/images/ic_shopping_trolley.svg"]', element_name="Shopping Cart Logo")
        self.utils.wait_loadingCircle(enable=True)



    def initialCheck(self):
        self.utils.click_js(xpath='//div[@data-automation-id="edit-cart-menu-button"]',element_name='3 Dots sub-menu')
        if self.utils.element_exists(xPath='//div[@data-automation-id="menu-save-button"]', element_name='Salveaza Button',enable_Logging=False) == True:
            self.utils.log_customStep(key='Salveaza button is visible and accessible by the user',status= 'Passed')
        else:
            self.utils.log_customStep(key='Salveaza button is visible and accessible by the user', status='Failed')


        if self.utils.element_exists(xPath='//div[@data-automation-id="show-cancel-popup"]', element_name='Anuleaza Button',enable_Logging=False) == True:
            self.utils.log_customStep(key='Anuleaza button is visible and accessible by the user',status= 'Passed')
        else:
            self.utils.log_customStep(key='Anuleaza button is visible and accessible by the user', status='Fail')


        if self.utils.element_exists(xPath='//div[@data-automation-id="menu-print-button"]', element_name='Vizualizeaza Button',enable_Logging=False) == True:
            self.utils.log_customStep(key='Vizualizeaza button is visible and accessible by the user',status= 'Passed')
        else:
            self.utils.log_customStep(key='Vizualizeaza button is visible and accessible by the user', status='Failed')


        if self.utils.element_exists(xPath='//div[@data-automation-id="menu-send-mail-button"]', element_name='Trimite E-mail Button',enable_Logging=False) == True:
            self.utils.log_customStep(key='Trimite E-mail button is visible and accessible by the user',status= 'Passed')
        else:
            self.utils.log_customStep(key='Trimite E-mail button is visible and accessible by the user', status='Failed')


    def acquisitionTypeCart(self,pageConfigureaza_acquisitionType):


        #self.utils.click_js(xpath='//button/img[@alt="shopping"]', element_name='Shopping Cart Logo')
        #self.utils.wait_loadingCircle(enable=True)
        value = self.utils.get_text(xPath='//div[@class="cart-item-title bold total-header"]/span/span[@class="wireless-tag"]/span', element_name='acquisitionTypeCart', enable_Logging=False)


        if pageConfigureaza_acquisitionType == 'generalAquisition':
            text = str(value.split(' #')[0])
            if text == "Număr Nou":
                self.utils.log_customStep(key='generalAquisition - The "Numar Nou" text is displayed', status='Passed')
            else:
                self.utils.log_customStep(key=f'generalAquisition - The "Numar Nou" text is displayed | In Cart is: {text}', status='Fail')

        elif pageConfigureaza_acquisitionType == 'MFP':
            split = str(value.split('(')[1])
            text = str(split.split(')')[0])
            if text == "Migrare la abonament":
                self.utils.log_customStep(key='MFP - The "Migrare la abonament" text is displayed', status='Passed')
            else:
                self.utils.log_customStep(key=f'MFP - The "Migrare la abonament" text is displayed | In Cart is: {text}', status='Fail')

        elif pageConfigureaza_acquisitionType == 'Portin postpaid':
            split = str(value.split('(')[1])
            text = str(split.split(')')[0])
            if text == "Portare la abonament":
                self.utils.log_customStep(key='Portin postpaid - The "Portare la abonament" text is displayed', status='Passed')
            else:
                self.utils.log_customStep(key=f'Portin postpaid - The "Portare la abonament" text is displayed | In Cart is: {text}', status='Fail')

        elif pageConfigureaza_acquisitionType == 'Portin prepaid':
            split = str(value.split('(')[1])
            text = str(split.split(')')[0])
            if text == "Portare la cartela":
                self.utils.log_customStep(key='Portin prepaid - The "Portare la cartela" text is displayed', status='Passed')
            else:
                self.utils.log_customStep(key=f'Portin prepaid - The "Portare la cartela" text is displayed | In Cart is: {text}', status='Fail')


    def shoppingCartOffer(self, pageConfigureaza_offerType):

        if pageConfigureaza_offerType == 'serviceOnly':
            values = self.utils.get_text(enable_Logging=False, xPath='//div[@class="mini-cart-menu item flex"]/div/div[@class="cart-item-title-nonCapitalize"]', element_name='Service Name')

            self.utils.setTempValue(key='serviceName', value=values)
            service = self.utils.get_text(xPath="//span[contains(text(),'SERVICII MOBIL')]//ancestor::div[1]/label",element_name='Nume Service', enable_Logging=False)
            if values == service:
                self.utils.log_customStep(key='The Products and Services are correclty displayed according to the Service Only rules', status='Passed')
            else:
                self.utils.log_customStep(key=f'The Products and Services are correclty displayed according to the Service Only rules \/ From Cart: "{values}" || From Page: "{service}"', status='Fail')

            # avansService = self.utils.get_text(xPath='(//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section/div//div[@class="item-right-wrap"]/div[@class="item-wrap-prices"]/div)[1]', element_name='Service Price - In avans',enable_Logging=False)
            # if avansService == '- - -':
            #     avansService = '0'
            # else:
            #     avansService = ((avansService.split('€'))[0]).replace(",", ".")

            time.sleep(10)
            lunarService = self.utils.get_text(xPath='//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section/div//div[@class="item-right-wrap"]/div[@class="item-wrap-prices"]/div', element_name='Service Price - Lunar',enable_Logging=False)
            if lunarService == '- - -':
                lunarService = '0'
            else:
                lunarService = ((lunarService.split('€'))[0]).replace(",", ".")

            # totalAvans = self.utils.get_text(xPath='(//section[@class="cart-total-items-section"]//div[@class="item-wrap-prices"]/div[@class="cart-price bold"])[1]', element_name='Total Price - Avans',enable_Logging=False)
            # if totalAvans == '- - -':
            #     totalAvans = '0'
            # else:
            #     totalAvans = ((totalAvans.split('€'))[0]).replace(",", ".")

            totalLunar = self.utils.get_text(xPath='//section[@class="cart-total-items-section"]//div[@class="item-wrap-prices"]/div[@class="cart-price bold"]', element_name='Total Price - Lunar',enable_Logging=False)
            if totalLunar == '- - -':
                totalLunar = '0'
            else:
                totalLunar = ((totalLunar.split('€'))[0]).replace(",", ".")
            time.sleep(10)
            if lunarService == totalLunar:
                self.utils.log_customStep(key='The total cart price is the sum of all the price values found in the mini-cart menu | Lunar',status='Passed')
            else:
                self.utils.log_customStep(key=f'The total cart price is the sum of all the price values found in the mini-cart menu \/ Lunar -  SumaTotal: "{lunarService}" || TotalAfisat: "{totalLunar}"',status='Fail')
            time.sleep(10)
            # if avansService == totalAvans:
            #     self.utils.log_customStep(key='The total cart price is the sum of all the price values found in the mini-cart menu | Avans',status='Passed')
            # else:
            #     self.utils.log_customStep(key=f'The total cart price is the sum of all the price values found in the mini-cart menu \/ Avans -  SumaTotal: "{avansService}" || TotalAfisat: "{totalAvans}"',status='Fail')

            # self.utils.setTempValue(key='sumaAvans', value=avansService)
            self.utils.setTempValue(key='sumalunar', value=lunarService)


        if pageConfigureaza_offerType == 'serviceAndDevice':
            values = self.utils.get_text(xPath='//div[@class="mini-cart-menu item flex"]/div/div[@class="cart-item-title-nonCapitalize"]', element_name='Service Name',enable_Logging=False)
            self.utils.setTempValue(key='serviceName', value=values)

            service = self.utils.get_text(xPath="//span[contains(text(),'SERVICII MOBIL')]//ancestor::div[1]/label",element_name='Nume Service', enable_Logging=False)
            if values == service:
                self.utils.log_customStep(key='The Products and Services are correclty displayed according to the Service Only rules', status='Passed')
            else:
                self.utils.log_customStep(key=f'The Products and Services are correclty displayed according to the Service Only rules \/ From Cart: "{values}" || From Page: "{service}"', status='Fail')
            time.sleep(10)

            valued = self.utils.get_text(xPath='//section[@class="cart-items-section"]/div/div/div/div/div[@class="cart-item-title"]', element_name='Device Name',enable_Logging=False)


            device = self.utils.get_text(xPath="//span[contains(text(),'TELEFOANE')]//ancestor::div[1]/label", element_name='Nume device', enable_Logging=False)
            if str(valued) == device:
                self.utils.log_customStep(key='The Products and Services are correclty displayed according to the Service Only rules',status='Passed')
            else:
                self.utils.log_customStep(key=f'The Products and Services are correclty displayed according to the Service Only rules \/ From Cart: "{valued}" || From Page: "{device}"',status='Fail')

            time.sleep(10)
            avansService = self.utils.get_text(xPath='(//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section/div//div[@class="item-right-wrap"]/div[@class="item-wrap-prices"]/div)[1]', element_name='Service Price - In avans',enable_Logging=False)
            if avansService == '- - -':
                avansService = '0'
            else:
                avansService = ((avansService.split('€'))[0]).replace(",", ".")


            lunarService = self.utils.get_text(xPath='(//div[@class="cart-accordion-panel show-cart-accordion-panel"]/section/div//div[@class="item-right-wrap"]/div[@class="item-wrap-prices"]/div)[2]', element_name='Service Price - Lunar',enable_Logging=False)
            if lunarService == '- - -':
                lunarService = '0'
            else:
                lunarService = ((lunarService.split('€'))[0]).replace(",", ".")

            avansDevice = self.utils.get_text(xPath='(//div[@class="mini-cart-menu flex"]//div[@class="item-wrap-prices"]/div)[1]', element_name='Device Price - In avans',enable_Logging=False)

            if avansDevice == '- - -':
                avansDevice = '0'
            else:
                avansDevice = ((avansDevice.split('€'))[0]).replace(",", ".")


            lunarDevice = self.utils.get_text(xPath='(//div[@class="mini-cart-menu flex"]//div[@class="item-wrap-prices"]/div)[2]', element_name='Device Price - Lunar',enable_Logging=False)

            if lunarDevice == '- - -':
                lunarDevice = '0'
            else:
                lunarDevice = ((lunarDevice.split('€'))[0]).replace(",", ".")

            totalAvans = self.utils.get_text(xPath='(//section[@class="cart-total-items-section"]//div[@class="item-wrap-prices"]/div[@class="cart-price bold"])[1]', element_name='Total Price - Avans',enable_Logging=False)
            if totalAvans == '- - -':
                totalAvans = '0'
            else:
                totalAvans = ((totalAvans.split('€'))[0]).replace(",", ".")

            totalLunar = self.utils.get_text(xPath='(//section[@class="cart-total-items-section"]//div[@class="item-wrap-prices"]/div[@class="cart-price bold"])[2]', element_name='Total Price - Avans',enable_Logging=False)

            if totalLunar == '- - -':
                totalLunar = '0'
            else:
                totalLunar = ((totalLunar.split('€'))[0]).replace(",", ".")


            sumalunar = format(sum(float(i) for i in [lunarService, lunarDevice]), '.2f')
            sumaAvans = format(sum(float(i) for i in [avansService, avansDevice]), '.2f')



            if sumalunar == totalLunar:
                self.utils.log_customStep(key='The total cart price is the sum of all the price values found in the mini-cart menu | Lunar',status='Passed')
            else:
                self.utils.log_customStep(key=f'The total cart price is the sum of all the price values found in the mini-cart menu \/ Lunar -  SumaTotal: "{sumalunar}" || TotalAfisat: "{totalLunar}"',status='Fail')

            if sumaAvans == totalAvans:
                self.utils.log_customStep(key='The total cart price is the sum of all the price values found in the mini-cart menu| Avans',status='Passed')
            else:
                self.utils.log_customStep(key=f'The total cart price is the sum of all the price values found in the mini-cart menu \/ Avans -  SumaTotal: "{sumaAvans}" || TotalAfisat: "{totalAvans}"',status='Fail')

            self.utils.setTempValue(key='sumalunar',value=sumalunar)
            self.utils.setTempValue(key='sumaAvans', value=sumaAvans)

    def anuleaza(self):

        ### Dezvolatarea pe aceasta ramura a fost blocata (30.06) --> pop -ul nu aparea (conform documentatie)   Defect ID #4016
        self.utils.log_customStep(key='!FAIL! The "Salvare comanda" pop-up is opened DefectID #4016 ', status='Fail')
        self.utils.click_js(xpath='//div[@data-automation-id="show-cancel-popup"]', element_name='Anuleaza Button')


    def salveaza(self):
        self.utils.click_js(xpath='//div[@data-automation-id="menu-save-button"]', element_name='Salveaza Button')

        if self.utils.element_exists(xPath='//div[@class="title-popup undefined title-height"]', element_name='Salveaza Pop-up',enable_Logging=False) == True:
            self.utils.log_customStep(key='The "Salvare comanda" pop-up is opened',status= 'Passed')
        else:
            self.utils.log_customStep(key='The "Salvare comanda" pop-up is opened', status='Fail')

    def salveazaAnuleaza(self):
        if self.utils.element_exists(xPath='//button[@data-automation-id="save-popup-cancel-button"]', element_name='Anuleaza Button - from Salveaza Pop-up', enable_Logging=False) == True and self.utils.is_element_Displayed(xPath='//button[@data-automation-id="save-popup-cancel-button"]', element_name='Anuleaza Button - from Salveaza Pop-up', enable_Logging=False) == True:
            self.utils.log_customStep(key='The button is visible and accessible to the user',status= 'Passed')
        else:
            self.utils.log_customStep(key='The button is visible and accessible to the user', status='Fail')

        self.utils.click_js(xpath='//button[@data-automation-id="save-popup-cancel-button"]', element_name='Anuleaza Button - from Salveaza Pop-up')

        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="title-popup undefined title-height"]', element_name='Salveaza Pop-up') == False:
            self.utils.log_customStep(key='The "Salveaza comanda" pop-up is closed',status= 'Passed')
        else:
            self.utils.log_customStep(key='The "Salveaza comanda" pop-up is closed', status='Fail')


        if self.utils.element_exists(xPath='//div[@class="subscribers-tabs configuration-tabs"]', element_name='Configutreaza panel') == True:
            self.utils.log_customStep(key='The user remains in the Configuration screen',status= 'Passed')
        else:
            self.utils.log_customStep(key='The user remains in the Configuration screen', status='Fail')

    def salveazaInchide(self, verificareDashboard):



        if self.utils.element_exists(xPath='//button[@data-automation-id="save-popup-save-button"]', element_name='Anuleaza Button - from Salveaza Pop-up', enable_Logging=False) == True and self.utils.is_element_Displayed(xPath='//button[@data-automation-id="save-popup-save-button"]', element_name='Anuleaza Button - from Salveaza Pop-up', enable_Logging=False) == True:
            self.utils.log_customStep(key='The button is visible and accessible to the user',status= 'Passed')
        else:
            self.utils.log_customStep(key='The button is visible and accessible to the user', status='Fail')

        ## verificareDashboard - self.utils.get_text(enable_Logging=False, xPath='//h4[@class="identity-number"]', element_name='CNP-ul Customerului')
        ## verificareDashboard - CNP ul Customerului, luat de pe pagina de Dashboard, la inceputul testului

        self.utils.click_js(xpath='//button[@data-automation-id="save-popup-save-button"]', element_name='Anuleaza Button - from Salveaza Pop-up')


        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="toaster-content"]', element_name='Toaster message') == True:
            self.utils.log_customStep(key='A toaster message informing the user that the order was saved is displayed on top of the screen',status= 'Passed')
        else:
            self.utils.log_customStep(key='A toaster message informing the user that the order was saved is displayed on top of the screen', status='Fail')

        verificareDashboard = str(verificareDashboard)
        verificare = str(self.utils.get_text(xPath='//h4[@class="identity-number"]', element_name='CNP-ul Customerului', enable_Logging=False))

        if verificare == verificareDashboard:
            self.utils.log_customStep(key='The user is redirected to the C360 screen of the user in context',status= 'Passed')
        else:
            self.utils.log_customStep(key='The user is redirected to the C360 screen of the user in context', status='Fail')





        orderBefore = self.utils.get_text(xPath='(//div[@class="description-font"]/div[@class="description-text"])[1]/span', element_name='order -ul rezultat',enable_Logging=False)

        order_Dasboard = str(orderBefore.split('#')[1])
        #order_Dasboard will be used below




        self.utils.click_js(xpath='//button[@class="link-title blue"]', element_name='Toate Comenzile')

        # order_agentOrders = self.utils.get_text(xPath='(//span[@class="order-list-text"])[2]')


        self.utils.click_js(xpath='//li[@data-automation-id="tab-tab-key-1"]', element_name='CAUTA')
        self.utils.wait_loadingCircle()

        # Expand CAUTA DUPA / select Order ID
        self.utils.click_js(xpath='(//div[@class="Select-control"])[1]')
        # time.sleep(2)
        self.utils.click_js(xpath='(//div[@class="Select-control"])[1]/div/div/span', element_name='Expand CAUTA DUPA')
        self.utils.type_js(xpath='(//div[@class="Select-control"])[1]/div[1]/div[2]/input', value='Order ID')

        # Show all
        self.utils.click_js(xpath='//label[@class="switch "]/span', element_name='Arata comenzile finalizate/anulate')

        # order ID
        self.utils.type_js(xpath='//input[@type="text"]', value=order_Dasboard)


# "Cauta Comanda" Enabled and Click
        cauta_com_button = self.utils.get_text(enable_Logging=False, xPath='//button[@class="primary bold right-align vdf-find-order-button "]/span')

        if self.utils.element_isClickable(enable_Logging=False, xpath='//button[@class="primary bold right-align vdf-find-order-button "]', element_name='CAUTA COMANDA') and 'caut' in cauta_com_button.lower() and 'comand' in cauta_com_button.lower():
            self.utils.log_customStep(key='CAUTA COMANDA Enabled', status='Passed')
            self.utils.click_js(xpath='//button[@class="primary bold right-align vdf-find-order-button "]', enable_Logging=False)
        else:
            self.utils.log_customStep(key='CAUTA COMANDA Enabled', status='Fail')

        self.utils.wait_loadingCircle()

        id_order = self.utils.get_text(xPath='(//span[@class="order-list-text"])[2]', enable_Logging=False)
        self.order_agentOrders = str(id_order.split('#')[1])


        if order_Dasboard == self.order_agentOrders:
            self.utils.log_customStep(key='The order is saved and can be searched for by using the My Orders functionality',status='Passed')
        else:
            self.utils.log_customStep(key='The order is saved and can be searched for by using the My Orders functionality',status='Fail')




    def trimiteMailCommon(self):

        self.utils.click_js(xpath='data-automation-id="menu-send-mail-button"', element_name='Trimite E-Mail Button')


        if self.utils.element_exists(xPath='//div[@class="wrap-popup "]', element_name='Trimite E-Mail Pop-up', enable_Logging=False) == True:
            self.utils.log_customStep(key='The "Trimite pe email" pop-up is opened',status= 'Passed')


            if self.utils.element_exists(xPath='//button[@class="confirmationButton primary bold"]', element_name='Trimite E-Mail - Trimite button', enable_Logging=False) == True:

                self.utils.log_customStep(key='If the "prenume", "nume" and "email" information exists in the database, the fields are automatically populated with the correct data', status='Passed')

            else:
                self.utils.log_customStep(key='If the "prenume", "nume" and "email" information exists in the database, the fields are automatically populated with the correct data', status='Fail')

                if self.utils.element_exists(enable_Logging=False, xPath='//button[@class="confirmationButton primary bold disabled"]', element_name='Trimite E-Mail - Trimite button Disable') == True:
                    self.utils.log_customStep(key='The "prenume", "nume" and "email" information is captured as mandatory information', status='Passed')

                    self.check_trimiteMail_button(inputElement='initial')


                    self.utils.type_js(xpath='//input[@name="personFirstName"]', value='Prenume-AutomationTest', element_name='Prenume - Trimite E-Mail')
                    self.check_trimiteMail_button(inputElement='prenume')

                    self.utils.type_js(xpath='//input[@name="personLastName"]', value='Prenume-AutomationTest',element_name='Nume - Trimite E-Mail')
                    self.check_trimiteMail_button(inputElement='nume')
                    self.check_trimiteMail_button(inputElement='emailBefore')

                    self.utils.type_js(xpath='//input[@name="contactEmail"]', value='automation@test.com',element_name='Prenume - Trimite E-Mail')
                    self.check_trimiteMail_button(inputElement='emailAfter')

                else:
                    self.utils.log_customStep(key='The "prenume", "nume" and "email" information is captured as mandatory information',status='Fail')
        else:
            self.utils.log_customStep(key='The "Trimite pe email" pop-up is opened', status='Fail')


    def check_trimiteMail_button(self, inputElement):

        trimite_button_click = self.utils.element_isClickable(xpath='(//button[@data-automation-id="save-popup-save-button"])[2]',enable_Logging=False)
        trimite_button_visible = self.utils.is_element_Displayed(xPath='(//button[@data-automation-id="save-popup-save-button"])[2]', enable_Logging=False)


        if trimite_button_click == False and trimite_button_visible == True:
            if inputElement in ['initial', 'prenume', 'nume', 'emailBefore']:
                self.utils.log_customStep(key=f'For <{inputElement}> field - The "Trimite" button is visible but disabled to the user until the mandatory information is captured',status='Passed')
            else:
                self.utils.log_customStep(key=f'For <{inputElement}> field - The "Trimite" button is visible but disabled to the user until the mandatory information is captured',status='Fail')

        elif trimite_button == True and trimite_button_visible == True:
            if inputElement in ['emailAfter']:
                self.utils.log_customStep(key=f'For <{inputElement}> field - The "Trimite" button is visible but disabled to the user until the mandatory information is captured',status='Passed')
            else:
                self.utils.log_customStep(key=f'For <{inputElement}> field - The "Trimite" button is visible but disabled to the user until the mandatory information is captured',status='Fail')



    def trimite_vizualizeaza_buttons(self, choise):
        trimite_button_click = self.utils.element_isClickable(xpath='(//button[@data-automation-id="save-popup-save-button"])[2]',enable_Logging=False)
        trimite_button_visible = self.utils.is_element_Displayed(xPath='(//button[@data-automation-id="save-popup-save-button"])[2]',enable_Logging=False)

        vizualizeaza_button_click = self.utils.element_isClickable(xpath='//div[@class="link-title blue uppercase inline-flex no-arrow"]',enable_Logging=False)
        vizualizeaza_button_visible = self.utils.is_element_Displayed(xPath='//div[@class="link-title blue uppercase inline-flex no-arrow"]',enable_Logging=False)

        if choise == 'trimite':
            if trimite_button_click == True and trimite_button_visible == True:
                self.utils.log_customStep(key='The button is visible and accessible to the user', status='Passed')
            else:
                self.utils.log_customStep(key='The button is visible and accessible to the user', status='Fail')

        elif choise == 'vizualizeaza':
            if vizualizeaza_button_click == True and vizualizeaza_button_visible == True:
                self.utils.log_customStep(key='The button is visible and accessible to the user', status='Passed')
            else:
                self.utils.log_customStep(key='The button is visible and accessible to the user', status='Fail')


    def trimiteMail(self):
        self.trimite_vizualizeaza_buttons(choise='trimite')
        self.utils.click_js(xpath='(//button[@data-automation-id="save-popup-save-button"])[2]', element_name='Trimite')

        if self.utils.element_exists(xPath='//div[@class="toaster-content"]', element_name='Toaster message',enable_Logging=False) == True:
            self.utils.log_customStep(key='A toaster message is displayed informing the user that the summary was successfully sent to the address',status= 'Passed')
        else:
            self.utils.log_customStep(key='A toaster message is displayed informing the user that the summary was successfully sent to the address', status='Fail')

        if self.utils.element_exists(xPath='//div[@class="wrap-popup "]', element_name='Trimite Pop-up',enable_Logging=False) == False:
            self.utils.log_customStep(key='The pop-up message is closed',status='Passed')
        else:
            self.utils.log_customStep(key='The pop-up message is closed',status='Fail')

        if self.utils.element_exists(xPath='//div[@class="subscribers-tabs configuration-tabs"]', element_name='Trimite Pop-up Closed',enable_Logging=False) == True:
            self.utils.log_customStep(key='The user remains in the Configuration screen',status= 'Passed')
        else:
            self.utils.log_customStep(key='The user remains in the Configuration screen', status='Fail')


    def vizualizeazaMail(self):
        self.trimite_vizualizeaza_buttons(choise='vizualizeaza')

        self.utils.click_js(xpath='//div[@class="link-title blue uppercase inline-flex no-arrow"]',element_name='Vizualizeaza Button')

        ##DE implementat 'A new tab is opened in the browser displaying the order summary detailed in PDF format#'#


        if self.utils.element_exists(xPath='//div[@class="wrap-popup "]', element_name='Trimite Pop-up',enable_Logging=False) == True:
            self.utils.log_customStep(key='The user remains in the pop-up',status='Passed')
        else:
            self.utils.log_customStep(key='The user remains in the pop-up',status='Fail')




        if self.utils.element_exists(xPath='//div[@class="cart-menu-wrap"]', element_name=' Shopping Cart menu',enable_Logging=False) == True:
            self.utils.log_customStep(key='The shopping cart remains open', status='Passed')
        else:
            self.utils.log_customStep(key='The shopping cart remains open', status='Fail')


    def vizualizeaza(self):
        self.utils.click_js(xpath='//div[@data-automation-id="menu-print-button"]', element_name='Vizualizeaza Button')

        #DE implementat TaB ul

        if self.utils.element_exists(xPath='//div[@class="cart-menu-wrap"]', element_name=' Shopping Cart menu',enable_Logging=False) == True:
            self.utils.log_customStep(key='The shopping cart remains open', status='Passed')
        else:
            self.utils.log_customStep(key='The shopping cart remains open', status='Fail')



    def miniCart(self):

        ## De intrebat cu selecteaza oferta SEBI F.

        ofertaConfigureaza = str(self.utils.get_text(xPath='//div[@class="accordion_head_title  "]//label[@class="text-normal"]', element_name='Nume oferta -Din Pagina',enable_Logging=False))

        ofertaCart = str(self.utils.get_text(xPath='//div[@class="mini-cart-menu item flex"]/div/div[@class="cart-item-title-nonCapitalize"]', element_name='Nume oferta -Din Cart',enable_Logging=False))


        self.utils.click_js(xpath='//img[@alt="shopping"]', element_name='Shoppin Cart')

        self.utils.hover(xpath='(//div[@class="mini-cart-menu item flex"]//div[@class="remove-link-container"])[1]', element_name='Trash can - Shopping Cart',enable_Logging=False)

        if self.utils.is_element_Displayed(xPath='(//div[@class="mini-cart-menu item flex"]//div[@class="remove-link-container"])[1]', element_name='Trash can - Shopping Cart',enable_Logging=False) == True:
            self.utils.log_customStep(key='The "Trash can" button is displayed', status='Passed')
        else:
            self.utils.log_customStep(key='The "Trash can" button is displayed', status='Fail')


        self.utils.click_js(xpath='(//div[@class="mini-cart-menu item flex"]//div[@class="remove-link-container"])[1]', element_name='Trash can - Shopping Cart')

        title = str(self.utils.get_text(xPath='//div[@class="title-popup undefined title-height"]/h3',enable_Logging=False))
        if self.utils.element_exists(xPath='//div[@class="title-popup undefined title-height"]/h3', element_name='Sterge produs Pop-up',enable_Logging=False) == True and 'produs' in title.lower():
            self.utils.log_customStep(key='The "Stergere produs" pop-up is displayed', status='Passed')
        else:
            self.utils.log_customStep(key='The "Stergere produs" pop-up is displayed', status='Fail')

        self.utils.click_js(xpath='//button[@class="confirmationButton primary bold"]', element_name='Sterge Produs - button')

        if self.utils.element_exists(xPath='//div[@class="title-popup undefined title-height"]/h3', element_name='Sterge produs Pop-up',enable_Logging=False) == False:
            self.utils.log_customStep(key='The pop-up is closed', status='Passed')
        else:
            self.utils.log_customStep(key='The pop-up is closed', status='Fail')

        if self.utils.element_exists(xPath='//div[@class="subscribers-tabs configuration-tabs"]', element_name='Configutreaza panel',enable_Logging=False) == True:
            self.utils.log_customStep(key='The user remains in the Configuration screen',status= 'Passed')
        else:
            self.utils.log_customStep(key='The user remains in the Configuration screen', status='Fail')


        if self.utils.element_isClickable(xpath='//button[@type="button" and @class="img-wrap"]',element_name='Shopping Cart',enable_Logging=False) == False:
            self.utils.log_customStep(key='The product selections are removed from the shopping cart and from the configuration screen', status='Passed')
        else:
            self.utils.log_customStep(key='The product selections are removed from the shopping cart and from the configuration screen', status='Fail')



# Sau vom folosi metoda de mai jos pentru a stabili ultimul pass

        # if self.utils.element_exists(xPath=)

    def continuaButton(self):

        self.utils.wait_loadingCircle()
        self.utils.click_js(xpath='//button[@data-automation-id="menu-continue-button"]', element_name='Continua Button - Shopping Cart')
        if self.utils.element_exists(xPath='//div[@class="title-popup undefined title-height"]', element_name='POP-ul Error',enable_Logging=False ):
            self.utils.log_customStep(key='!CRITICAL FAIL! DEX Error ...ultimul telefon a fost rezervat de un alt agent... ', status='Fail')

        time.sleep(5)
        # self.utils.click_js(xpath='//button[@class="footer-btn-primary"]', element_name='Continua Button')

        self.utils.wait_loadingCircle()
        if self.utils.element_exists(xPath='//div[@class="personal-details-exisit"]', element_name='customerDetails screen',enable_Logging=False) == True:

            self.utils.log_customStep(key='The user is taken to the customerDetails screen', status='Passed')
        else:
            self.utils.log_customStep(key='The user is taken to the customerDetails screen', status='Fail')




class testFunctions_CustomerDetails:

    def __init__(self, utils):
        self.utils = utils


    def newCustomer(self):

        self.utils.type_js(xpath='//input[@data-automation-id="E-MAIL CONTACT"]',element_name='E-Mail de contact', value='emaildecontact_automation@test.com')
        # self.utils.type_js(xpath='//input[@data-automation-id="TELEFON ALTERNATIV"]',element_name='Telefon alternatic',value='0773456231')
        self.utils.click_js(xpath='//span[@class="checkmark "]',element_name='Telefon alternativ - Checkmark')

    def detaliiClient(self):

        if self.utils.element_exists(xPath='//div[@class="personal-details-title"]/h2[contains(text(),"DETALII CLIENT")]', element_name='Detalii client - CustomerDetails_Page ', enable_Logging=False) == True:
            nume = self.utils.get_text(xPath='(//div[@class="personal-details"]//div[@class="item"])[2]', element_name='Nume Client', enable_Logging=False)
            email = self.utils.get_text(xPath='(//div[@class="personal-details"]//div[@class="item personal-address"]/div)[1]', element_name='Email Client', enable_Logging=False)
            telefon = self.utils.get_text(xPath='(//div[@class="personal-details"]//div[@class="item"])[3]', element_name='Telefon Client', enable_Logging=False)
            # telefon = telefon.split(' ')[3]
            telefon_alternativ = self.utils.get_text(xPath='//div[@class="personal-details"]//div[@class="item empty-additional-phone-title"]', element_name='Telefon Alternativ Client', enable_Logging=False)
            # telefon_alternativ = (telefon_alternativ.split(' ')[2])
            print(telefon_alternativ,'@@@@@@@telefonAlt@@@@')
            adresa = self.utils.get_text(xPath='//div[@class="personal-details"]//div[@class="personal-ellipsis-style"]', element_name='Adresa Client', enable_Logging=False)

            self.utils.setTempValue(key="numeCustomerDetails", value=nume)
            self.utils.setTempValue(key="emailCustomerDetails", value=email)
            self.utils.setTempValue(key="telefonCustomerDetails", value=telefon)
            self.utils.setTempValue(key="telefon_alternativCustomerDetails", value=telefon_alternativ)
            self.utils.setTempValue(key="adresaCustomerDetails", value=adresa)

        else:
            self.utils.log_customStep(key='!FAIL! Detalii Client Field (From CustomerDetails) does not exists', status='Fail')


    def customerDetails(self):
        n = 0
        e = 0
        t = 0
        ta = 0
        a = 0
        print('Dashboard-->',self.utils.getTempValue(key='numeDashboard'),'   || Customer Details--> ',  self.utils.getTempValue(key='numeCustomerDetails'))
        print('Dashboard-->',self.utils.getTempValue(key='emailDashboard'),'   || Customer Details--> ',  self.utils.getTempValue(key='emailCustomerDetails'))

        print('Dashboard-->',self.utils.getTempValue(key='adresaCustomerDetails'),'   || Customer Details--> ',  self.utils.getTempValue(key='adresaDashboard'))

        print('Dashboard-->',self.utils.getTempValue(key='telefonDashboard'),'   || Customer Details--> ',  self.utils.getTempValue(key='telefonCustomerDetails'))

        print('Dashboard-->',self.utils.getTempValue(key='telefon_alternativDashboard'),'   || Customer Details--> ',  self.utils.getTempValue(key='telefon_alternativCustomerDetails'))

        if self.utils.getTempValue(key='numeDashboard') == self.utils.getTempValue(key='numeCustomerDetails'):
            n = 1

        if self.utils.getTempValue(key='emailDashboard') == self.utils.getTempValue(key='emailCustomerDetails'):
            e = 1

        if self.utils.getTempValue(key='telefonDashboard') ==  self.utils.getTempValue(key='telefonCustomerDetails'):
            t = 1

        if self.utils.getTempValue(key='telefon_alternativCustomerDetails') == self.utils.getTempValue(key='telefon_alternativDashboard'):
            ta = 1

        if self.utils.getTempValue(key='adresaCustomerDetails') == self.utils.getTempValue(key='adresaDashboard'):
            a = 1
        # print(n,'\n',e,'\n',t,'\n',ta,'\n',a,'\n')

        if n == 1 and e == 1 and t == 1 and ta == 1 and a == 1:
            self.utils.log_customStep(key='The details should be the same as in the 360 dashboard', status='Passed')

        if n == 0:
            self.utils.log_customStep(key=f"The details should be the same as in the 360 dashboard \/ Dashboar: {self.utils.getTempValue(key='numeDashboard')}  || CustomerDetails: {self.utils.getTempValue(key='numeCustomerDetails')}", status='Fail')

        if e == 0:
            self.utils.log_customStep(key=f"The details should be the same as in the 360 dashboard \/ Dashboar: {self.utils.getTempValue(key='emailDashboard')}  || CustomerDetails: {self.utils.getTempValue(key='emailCustomerDetails')}", status='Fail')

        if t == 0:
            self.utils.log_customStep(key=f"The details should be the same as in the 360 dashboard \/ Dashboar: {self.utils.getTempValue(key='telefonDashboard')}  || CustomerDetails: {self.utils.getTempValue(key='telefonCustomerDetails')}", status='Fail')

        if ta == 0:
            self.utils.log_customStep(key=f"The details should be the same as in the 360 dashboard \/ Dashboar: {self.utils.getTempValue(key='telefon_alternativDashboard')}  || CustomerDetails: {self.utils.getTempValue(key='telefon_alternativCustomerDetails')}", status='Fail')

        if a == 0:
            self.utils.log_customStep(key=f"The details should be the same as in the 360 dashboard \/ Dashboar: {self.utils.getTempValue(key='adresaDashboard')}  || CustomerDetails: {self.utils.getTempValue(key='adresaCustomerDetails')}", status='Fail')


    def generalAquisition(self):

        if self.utils.element_exists(xPath='//div[@class="personal-details-title"]/h2/span',element_name='Utilizator si numar',enable_Logging=False) == True:
            number = self.utils.get_text(xPath='//div[@class="table-cell top"]/div[@class="simple-text block"]/span',
                                         element_name='Numar nou',enable_Logging=False)
            newNumber = number.replace(' ', '')
            if len(newNumber) == 9 and newNumber[0] == '7':
                self.utils.log_customStep(key='Verify that the MSISDN is displayed in the "NEW MSISDN" field',
                                          status='Passed')
            else:
                self.utils.log_customStep(key='Verify that the MSISDN is displayed in the "NEW MSISDN" field',
                                          status='Fail')

            self.utils.click_js(xpath='//div[@class="link-title blue uppercase no-arrow"]/h6',
                                element_name='Schimba numar')
            self.utils.wait_loadingCircle(enable=True)
            if self.utils.element_exists(xPath='//div[@class="title-popup undefined title-height"]',element_name='Alocare numar',enable_Logging=False) == True:
                self.utils.log_customStep(key='The "ALOCARE NUMaR | Numar Nou #1" dialog box is displayed', status='Passed')

                self.utils.click_js(xpath='//div[@data-automation-id="number-allocation-popup-number-button"]', element_name='Numar')

                self.utils.click_js(xpath='//button[@class="primary  bold right-align"]', element_name='Schimba numar - Continua')

                self.utils.wait_loadingCircle(enable=True)
            else:
                self.utils.log_customStep(key='The "ALOCARE NUMaR | Numar Nou #1" dialog box is displayed',  status='Fail')

        else:
            self.utils.log_customStep(key='Verify that the MSISDN is displayed in the "NEW MSISDN" field',status='Fail')

    def MFP_YES(self):

        currentDay = date.today().strftime('%d')

        self.utils.wait_loadingCircle()
        self.utils.click_js(xpath='//div[@class="react-datepicker__input-container"]/input',element_name='Data activare numar')

        if self.utils.element_exists(xPath='//div[@class="react-datepicker__month-container"]//div[@tabindex="0"]', element_name='Date',enable_Logging=False) == True:
            self.utils.log_customStep(key='The date is selected', status='Passed')

        else:
            self.utils.log_customStep(key='The date is selected', status='Fail')


        if currentDay[0] == '0':
            currentDay = currentDay[1]
        calendarDay = self.utils.get_text(xPath='//div[@class="react-datepicker__month-container"]//div[@tabindex="0"]',element_name='Selected date',enable_Logging=False)
        calendarDayMonth = self.utils.get_text(xPath='//div[@class="react-datepicker__current-month"]', element_name='Selected year/month',enable_Logging=False)
        print(calendarDayMonth,'  *****************')
        calendarYear = calendarDayMonth.split(" ")[1]
        calendarMonth = calendarDayMonth.split(" ")[0]
        currentYear = date.today().strftime('%Y')
        currentMonth = date.today().strftime('%m')

        if currentMonth[0] == '0':
            currentMonth = currentMonth[1]

        dict = {"Ianuarie": "1",
                "Februare": "2",
                "Martie": "3",
                "Aprilie": "4",
                "Mai": "5",
                "Iunie": "6",
                "Iulie": "7",
                "August": "8",
                "Septembrie": "9",
                "Octombrie": "10",
                "Noiembrie": "11",
                "Decembrie": "12"
                }

        for key, val in dict.items():
            if calendarMonth == key:
                calendarMonth = val

        if int(calendarDay) == int(currentDay):
            if int(calendarMonth) == int(currentMonth):
                if int(calendarYear) == int(currentYear):
                    self.utils.log_customStep(key='Verify that the default date is sysdate', status='Passed')
                else:
                    self.utils.log_customStep(key='Year - Verify that the default date is sysdate', status='Fail')
            else:
                self.utils.log_customStep(key='Month - Verify that the default date is sysdate', status='Fail')
        else:
            self.utils.log_customStep(key='Day - Verify that the default date is sysdate', status='Fail')

        # start_date = "10/10/11"
        #
        # date_1 = datetime.datetime.strptime(start_date, "%m/%d/%y")
        #
        # print(date_1)
        #
        # end_date = date_1 + datetime.timedelta(days=30)
        #TBD
        self.utils.log_customStep(key='User is able to select a date between today and no further than 30 days in the future', status='Fail')


    def acquisitionType(self, pageCustomerDetails_acquisitionType):

        self.utils.wait_loadingCircle()
        if pageCustomerDetails_acquisitionType == 'Portin postpaid':
            if self.utils.element_exists(xPath='//input[@name="customerId"]', element_name='Cod Client Donor',enable_Logging=False) == True:
                self.utils.type_js(xpath='//input[@name="customerId"]',element_name='Cod Client Donor', value='1234!@utomation')
                self.utils.log_customStep(key='User is able to fill in the field by typing any type of characters (alphanumeric, special)',status='Passed')
            else:
                self.utils.log_customStep(key='User is able to fill in the field by typing any type of characters (alphanumeric, special)',status='Fail')

        elif pageCustomerDetails_acquisitionType == 'Portin prepaid':
            if self.utils.element_exists(xPath='//input[@name="existingSim"]', element_name='Existing SIM',enable_Logging=False) == True:

                # # 'getSID': 'http://devops02.connex.ro:8030/getSID/{}', TBD!!
                # self.utils.log_customStep(key='iulian.py acquisitionType SIM - API CALL', status='Fail')

                sim = self.utils.getURLApi(url='http://devops03.connex.ro:8030/getPrereq/get_SIM/', env="env=" + get_endPoint(endPoints.dexEnv, "CM_ENV"), key="RES")
                # url = 'http://devops03.connex.ro:8030/getPrereq/get_SIM/'
                # env = "env=CM20"
                # print(url + `env)

                if len(str(sim)) > 1:
                    self.utils.type_js(xpath='//input[@name="existingSim"]',element_name='Existing SIM', value=sim)
                    self.utils.log_customStep(key='User is able to fill in the details',status='Passed')
                else:
                    self.utils.log_customStep(key='User is able to fill in the details | Null response from SIM API' , status='Fail')
            else:
                self.utils.log_customStep(key='User is able to fill in the details',status='Fail')



    def permanent(self):

        if self.utils.element_exists(xPath='//input[@class="wrap-input-radio" and @value="committed"]', element_name='tip contract',enable_Logging=False) == True and self.utils.element_exists(xPath='//label[@class="switch disabled"]', element_name='For check',enable_Logging=False) == True:
            self.utils.log_customStep(key='This is the default option and no change is required', status='Passed')
            number = self.utils.get_text(xPath='//div[@class="table-cell top"]/div[@class="simple-text block"]/span', element_name='Numar nou',enable_Logging=False)
            newNumber = number.replace(' ', '')
            if len(newNumber) == 9 and newNumber[0] == '7':
                self.utils.log_customStep(key='A MSISDN is automatically allocated', status='Passed')
            else:
                self.utils.log_customStep(key='A MSISDN is automatically allocated', status='Fail')

        else:
            self.utils.log_customStep(key='This is the default option and no change is required', status='Fail')


    def random(self):

        self.utils.click_js(xpath='//div[@class="link-title blue uppercase no-arrow"]', element_name='Schimba numar')
        self.utils.wait_loadingCircle(enable=True)
        if self.utils.element_exists(xPath='//div[@class="number-box selected "]', element_name='Numar selectat',enable_Logging=False) == True:
            self.utils.click_js(xpath='(//div[@class="number-box  "])[1]',element_name='Numar selectat nou')
            self.utils.log_customStep(key='The user is able to select the number and the number is highlighted', status='Passed')
        else:
            self.utils.log_customStep(key='The user is able to select the number and the number is highlighted', status='Fail')

        self.utils.wait_loadingCircle()

        self.utils.click_js(xpath='//button[@class="primary  bold right-align"]', element_name='Continua button')

        self.MFP_YES()

        self.utils.log_customStep(key='User is able to select a date', status='Fail')



    def customized(self):

        self.utils.click_js(xpath='//div[@class="link-title blue uppercase no-arrow"]', element_name='Schimba numar')
        self.utils.wait_loadingCircle(enable=True)

        self.utils.click_js(xpath='//div[@data-automation-id="number-allocation-popup-special-number-button"]', element_name='Numar special')
        if self.utils.element_exists(xPath='//form[@class="special-number-form"]', element_name='Numar special form',enable_Logging=False) == True:
            self.utils.log_customStep(key='The "ALOCARE NUMaR | Numar Nou #1" dialog is displayed', status='Passed')
        else:
            self.utils.log_customStep(key='The "ALOCARE NUMaR | Numar Nou #1" dialog is displayed', status='Fail')
        self.utils.wait_loadingCircle(enable=True)

        self.utils.type_js(xpath='//input[@class="special-number-form-input "]', element_name='Cauta numar special', value='x1')
        self.utils.click_js(xpath='//div[@class="special-number-form-find "]', element_name='Cauta button')
        self.utils.wait_loadingCircle()


        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="special-number-result"]', element_name='Special number | |') == True:
            self.utils.log_customStep(key='For example, X1', status='Passed')
        else:
            self.utils.log_customStep(key='For example, X1', status='Fail')


        # Below -- FROM random()

        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="number-box selected "]', element_name='Numar selectat') == True:
            self.utils.click_js(xpath='(//div[@class="special-number-result"]//div[@class="number-box  "])[1]', element_name='Special number selected')
            self.utils.log_customStep(key='The user is able to select the number and the number is highlighted', status='Passed')
        else:
            self.utils.log_customStep(key='The user is able to select the number and the number is highlighted', status='Fail')

        self.utils.wait_loadingCircle()
        self.utils.click_js(xpath='//button[@class="primary  bold right-align"]', element_name='Continua button')

        self.MFP_YES()

        self.utils.log_customStep(key='User is able to select a date', status='Fail')



    def temporaryYes(self):
        self.utils.click_js(xpath='//div[@class="wrap-checkbox padding-left-5"]/input', element_name='Asteapta portarea')

        if self.utils.element_exists(enable_Logging=False, xPath='//label[@class="switch disabled"]', element_name='For check') == False:

            self.utils.log_customStep(key='No user action required, the "Aloca numar temporar" option is toggled on by default', status='Passed')
            number = self.utils.get_text(enable_Logging=False, xPath='//div[@class="table-cell top"]/div[@class="simple-text block"]/span', element_name='Numar nou')
            newNumber = number.replace(' ', '')
            if len(newNumber) == 9 and newNumber[0] == '7':
                self.utils.log_customStep(key='A MSISDN is automatically allocated', status='Passed')
            else:
                self.utils.log_customStep(key=f'A MSISDN is automatically allocated \/ MSISDN FROM Page: {newNumber}', status='Fail')

        else:
            self.utils.log_customStep(key='No user action required, the "Aloca numar temporar" option is toggled on by default', status='Fail')


    def temporaryNo(self):

        if self.utils.element_exists(enable_Logging=False, xPath='class="wrap-input input-error wrap-row uniqueNumber"', element_name='Sim Field') == True:
            self.utils.log_customStep(key='Verify that the "Serie SIM" field is blank by default before inserting the number', status='Passed')
        else:
            self.utils.log_customStep(key='Verify that the "Serie SIM" field is blank by default before inserting the number', status='Fail')

        ###CALL DE API sim
        if self.utils.element_exists(enable_Logging=False, xPath='//input[@name="existingSim"]', element_name='Existing SIM') == True:

            value = self.utils.getURLApi(url='http://devops03.connex.ro:8030/getPrereq/get_SIM/', env="env=" + get_endPoint(endPoints.dexEnv, "CM_ENV"), key="RES")
            # url = 'http://devops03.connex.ro:8030/getPrereq/get_SIM/'
            # env = "env=CM20"
            # print(url + env)

            if len(str(value)) == 19:
                self.utils.type_js(xpath='//input[@name="existingSim"]', element_name='Existing SIM', value=value)
                self.utils.log_customStep(key='A valid SIM number of 19 characters is inserted and correctly displayed', status='Passed')
        else:
            self.utils.log_customStep(key='A valid SIM number of 19 characters is inserted and correctly displayed', status='Fail')

        if self.utils.element_exists(enable_Logging=False, xPath='//label[@class="switch disabled"]', element_name='For check') == True:
            self.utils.log_customStep(key='The button is toggled off', status='Passed')
        else:
            self.utils.log_customStep(key='The button is toggled off', status='Fail')

        # select a min and max date #TBD
        self.utils.log_customStep(key='temporaryNo method', status='Fail')




    def existingNotifNo(self):

        self.utils.click_js(xpath='//div[@class="link-title blue uppercase inline-flex no-arrow"]', element_name='Modifica numar')
        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup attention-a-notification-popup"]', element_name='Modifica pop-up') == True:
            self.utils.log_customStep(key='User is able to open the dialog', status='Passed')
        else:
            self.utils.log_customStep(key='User is able to open the dialog', status='Fail')

        value = '724342123'
        self.utils.type_js(xpath='//input[@name="notificationPhoneNumber"]', element_name='Numar Telefon', value=value)

        if self.utils.element_exists(enable_Logging=False, xPath='class="wrap-input wrap-row"',element_name='Pop-ul Midifica numar') == True:
            self.utils.log_customStep(key='User is able to change the number', status='Passed')
        else:
            self.utils.log_customStep(key='User is able to change the number', status='Fail')

        self.utils.click_js(xpath='//button[@data-automation-id="attention-a-notification-popup-continue-button"]', element_name='Salveaza button')

        self.utils.wait_loadingCircle()

        text = self.utils.get_text(enable_Logging=False, xPath='//div[@class="simple-text comment"]/span',element_name='Numar nou')
        lst = text.split(" ", 7)
        newNumber = lst[7].replace(' ', '')

        if str(newNumber) == str(value):
            self.utils.log_customStep(key='The new number is displayed correctly in the "Detalii" page', status='Passed')
        else:
            self.utils.log_customStep(key='The new number is displayed correctly in the "Detalii" page', status='Fail')


    def notifActionAnuleaza(self):

        text = self.utils.get_text(enable_Logging=False, xPath='//div[@class="simple-text comment"]/span',element_name='Numar nou')
        lst = text.split(" ", 7)
        oldNumber = lst[7].replace(' ', '')

        self.utils.click_js(xpath='//div[@class="link-title blue uppercase inline-flex no-arrow"]', element_name='Modifica numar')

        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup attention-a-notification-popup"]', element_name='Modifica pop-up') == True:
            self.utils.log_customStep(key='User is able to open the dialog', status='Passed')
        else:
            self.utils.log_customStep(key='User is able to open the dialog', status='Fail')

        self.utils.click_js(xpath='//button[@data-automation-id="attention-a-notification-popup-cancel-button"]', element_name='Anuleaza Button')
        self.utils.wait_loadingCircle()

        text = self.utils.get_text(enable_Logging=False, xPath='//div[@class="simple-text comment"]/span',element_name='Numar nou')
        lst = text.split(" ", 7)
        newNumber = lst[7].replace(' ', '')

        if str(newNumber) == str(oldNumber) and self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup attention-a-notification-popup"]', element_name='Modifica pop-up') == False:
            self.utils.log_customStep(key='The dialog box is closed and no change saved', status='Passed')
        else:
            self.utils.log_customStep(key='The dialog box is closed and no change saved', status='Fail')

    def notifActionX(self):

        text = self.utils.get_text(enable_Logging=False, xPath='//div[@class="simple-text comment"]/span', element_name='Numar nou')
        print(text.split(" ", 7),"----")
        lst = text.split(" ", 7)
        oldNumber = lst[7].replace(' ', '')
        self.utils.click_js(xpath='//div[@class="link-title blue uppercase inline-flex no-arrow"]',element_name='Modifica numar')

        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup attention-a-notification-popup"]',element_name='Modifica pop-up') == True:
            self.utils.log_customStep(key='User is able to open the dialog', status='Passed')
        else:
            self.utils.log_customStep(key='User is able to open the dialog', status='Fail')

        self.utils.click_js(xpath='//img[@class="x close-position"]', element_name='X Button')
        self.utils.wait_loadingCircle()

        text = self.utils.get_text(enable_Logging=False, xPath='//div[@class="simple-text comment"]/span', element_name='Numar nou')
        lst = text.split(" ", 7)
        newNumber = lst[7].replace(' ', '')

        if str(newNumber) == str(oldNumber) and self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup attention-a-notification-popup"]', element_name='Modifica pop-up') == False:
            self.utils.log_customStep(key='The dialog box is closed and no change saved', status='Passed')
        else:
            self.utils.log_customStep(key='The dialog box is closed and no change saved', status='Fail')


    def userTypeNo(self):
        self.utils.click_js(xpath='//button[@class="button-wrap-img toggle-popup padding-left-10"]', element_name='Informatii Utilizator')

        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup "]', element_name='Informatii utilizator pop-up') == True:

            lst = [1, 2]
            titlu = random.choice(lst)


            if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="mask-popup "]//select[@name="personTitle"]', element_name='Titlu check'):
                self.utils.dropDownSelector_Index(xpath='//div[@class="mask-popup "]//select[@name="personTitle"]', index=titlu)
                elemente = self.utils.get_numberOfElements(xPath='//div[@class="wrap-popup "]//select[@name="personTitle"]/option', enable_Logging=False)

                if int(elemente) < 4:
                    self.utils.log_customStep(key=f'Choose a title from "DL", "DNA", "CN" and "DNA/DL" | Only {elemente} choices available, instead of 4', status='Fail')
                else:
                    self.utils.log_customStep(key='Choose a title from "DL", "DNA", "CN" and "DNA/DL"', status='Passed')

            else:
                self.utils.log_customStep(key='Choose a title from "DL", "DNA", "CN" and "DNA/DL"', status='Fail')

            if titlu == 1:
                prenume = 'Marian'
                nume = 'Dragomir'
            elif titlu == 2:
                prenume = 'Mariana'
                nume = 'Dragomir'

            self.utils.type_js(xpath='//div[@class="mask-popup "]//input[@name="personFirstName"]',value='1231!@', tab=True)
            if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-input input-error md-name"]', element_name='Prenume Check'):
                self.utils.log_customStep(key='Only letters can be used', status='Passed')
                self.utils.type_js(xpath='//div[@class="mask-popup "]//input[@name="personFirstName"]', value=prenume)
            else:
                self.utils.log_customStep(key='Only letters can be used', status='Fail')

            self.utils.type_js(xpath='//div[@class="mask-popup "]//input[@name="personLastName"]', value='333!@', tab=True)
            if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-input input-error md-name"]', element_name='Nume Check'):
                self.utils.log_customStep(key='Only letters can be used', status='Passed')
                self.utils.type_js(xpath='//div[@class="mask-popup "]//input[@name="personFirstName"]', value=nume)

            else:
                self.utils.log_customStep(key='Only letters can be used', status='Fail')

            self.utils.type_js(xpath='//div[@class="mask-popup "]//input[@name="contactEmail"]', value='testautomatio', tab=True)
            if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-input input-error"]', element_name='Email Check'):
                self.utils.type_js(xpath='//div[@class="mask-popup "]//input[@name="contactEmail"]', value='testautomatio@aaa.com', tab=True)
                self.utils.log_customStep(key='Valid format, no errors (a@a.com)', status='Passed')
            else:
                self.utils.log_customStep(key='Valid format, no errors (a@a.com)', status='Fail')

            self.utils.type_js(xpath='//div[@class="mask-popup "]//input[@name="contactPhone"]', value='744444444')


    def minorYes(self):
        self.utils.click_js(xpath='//input[@class="wrap-input-radio" and @name="minor" and @value="true"]', element_name='Minor DA')

        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-row birth-date-component"]', element_name='Data Nasterii') == True:
            self.utils.log_customStep(key='The "Data nasterii" row is displayed in the "Utilizatorul numarului este titularul" section', status='Passed')
        else:
            self.utils.log_customStep(key='The "Data nasterii" row is displayed in the "Utilizatorul numarului este titularul" section', status='Fail')

        self.utils.dropDownSelector_By_visible_text(xpath='//div[@class="wrap-popup "]//select[@name="birthDay"]', value='12')
        self.utils.dropDownSelector_By_visible_text(xpath='//div[@class="wrap-popup "]//select[@name="birthDateMonth"]', value='12')
        self.utils.dropDownSelector_By_visible_text(xpath='//div[@class="wrap-popup "]//select[name="birthDateYear"]', value='2010')

        self.utils.type_js(xpath='//div[@class="wrap-popup "]//input[@name="contactEmail"]', value='contact@automation.com')

        self.utils.type_js(xpath='//div[@class="wrap-popup "]//input[@name="contactPhone"]',value='734333333')
        self.utils.click_js(xpath='//button[@class="confirmationButton primary bold"]', element_name='Salveaza Button')
        self.utils.wait_loadingCircle()
        self.userTypeYes()

    def minorNo(self):
        self.utils.click_js(xpath='//button[@class="confirmationButton primary bold"]', element_name='Salveaza Button')
        self.utils.wait_loadingCircle()
        self.userTypeYes()

    def userTypeYes(self):

        # TBD
        self.utils.log_customStep(key='Verify that details from "DETALII FACTURARE" are the correct', status='Fail')


    def newBillingYes(self):

        self.utils.click_js(xpath='//div[@class="undefined link-title blue"]', element_name='Cont Nou')
        self.utils.wait_loadingCircle()

        if self.utils.element_isClickable(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[1]', element_name='Tara', enable_Logging=False):
            self.utils.type_js(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[1]', element_name='Tara', value='ROMANIA')
            self.utils.log_customStep(key='Choose "Romania" as country', status='Passed')
        else:
            self.utils.log_customStep(key='Choose "Romania" as country', status='Fail')

        if self.utils.element_isClickable(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[2]', element_name='Judet', enable_Logging=False):
            self.utils.type_js(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[2]', element_name='Judet', value='Sector 3', forLoopPause=0.5)
            self.utils.log_customStep(key='The county can be selected', status='Passed')
        else:
            self.utils.log_customStep(key='The county can be selected', status='Fail')

        if self.utils.element_isClickable(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[3]', element_name='Localitate', enable_Logging=False):
            self.utils.type_js(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[3]', element_name='Localitate', value='Bucuresti', forLoopPause=0.5)
            self.utils.log_customStep(key='The city is selected', status='Passed')
        else:
            self.utils.log_customStep(key='The city is selected', status='Fail')

        if self.utils.element_isClickable(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[4]', element_name='Tip Strada', enable_Logging=False):
            self.utils.type_js(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[4]', element_name='Tip Strada', value='Str.', forLoopPause=0.5)
            self.utils.log_customStep(key='The street type can be selected', status='Passed')
        else:
            self.utils.log_customStep(key='The street type can be selected', status='Fail')

        if self.utils.element_isClickable(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[5]', element_name='Nume Strada', enable_Logging=False):
            self.utils.type_js(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[5]', element_name='Nume Strada', value='Schitului', forLoopPause=0.5, tab=True)
            self.utils.log_customStep(key='The street can be selected', status='Passed')
            self.utils.log_customStep(key='The street is displayed', status='Passed')
            self.utils.log_customStep(key='The user can search for the street', status='Passed')

        else:
            self.utils.log_customStep(key='The street can be selected', status='Fail')
            self.utils.log_customStep(key='The street is displayed', status='Fail')
            self.utils.log_customStep(key='The user can search for the street', status='Fail')




    def existingstreetYN(self):

        if self.utils.element_exists(enable_Logging=False, xPath='//label[@class="errorMessage-label"]', element_name="nume Strada error") == True:
            if self.utils.element_isClickable(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[5]', element_name='Nume Strada') == True:
                self.utils.type_js(xpath='(//div[@class="Select-control"]/div[@class="Select-multi-value-wrapper"])[5]', element_name='Nume Strada', value='Istriei', forLoopPause=0.5, tab=True)
                self.utils.log_customStep(key='The street can be selected', status='Passed')
                self.utils.log_customStep(key='The street is displayed', status='Passed')
                self.utils.log_customStep(key='The user can search for the street', status='Passed')

            else:
                self.utils.log_customStep(key='The street can be selected', status='Fail')
                self.utils.log_customStep(key='The street is displayed', status='Fail')
                self.utils.log_customStep(key='The user can search for the street', status='Fail')

        if self.utils.element_isClickable(xpath='//input[@name="streetNumber"]', element_name='Numar Strada') == True:
            self.utils.type_js(xpath='//input[@name="streetNumber"]', element_name='Numar Strada', value='10', forLoopPause=0.5)
            self.utils.log_customStep(key='The number and be typed in and is correctly displayed', status='Passed')
        else:
            self.utils.log_customStep(key='The number and be typed in and is correctly displayed', status='Fail')

        if self.utils.element_isClickable(xpath='//input[@name="blockNumber"]', element_name='Bloc') == True:
            self.utils.type_js(xpath='//input[@name="blockNumber"]', element_name='Bloc', value='6F1', forLoopPause=0.5, tab=True)
            self.utils.log_customStep(key='The value and be typed in and is correctly displayed', status='Passed')
        else:
            self.utils.log_customStep(key='The value and be typed in and is correctly displayed', status='Fail')

        if self.utils.element_isClickable(xpath='//input[@name="blockEntrance"]', element_name='Scara') == True:
            self.utils.type_js(xpath='//input[@name="blockEntrance"]', element_name='Scara', value='B', forLoopPause=0.5, tab=True)
            self.utils.log_customStep(key='The value and be typed in and is correctly displayed', status='Passed')
        else:
            self.utils.log_customStep(key='The value and be typed in and is correctly displayed', status='Fail')

        if self.utils.element_isClickable(xpath='//input[@name="apartment"]', element_name='Apartament') == True:
            self.utils.type_js(xpath='//input[@name="apartment"]', element_name='Apartament', value='25', forLoopPause=0.5, tab=True)
            self.utils.log_customStep(key='The value and be typed in and is correctly displayed', status='Passed')
        else:
            self.utils.log_customStep(key='The value and be typed in and is correctly displayed', status='Fail')

        self.utils.log_customStep(key='COD POSTA - The value is correctly displayed', status='Fail')

    def tiparire(self):
        self.utils.click_js(xpath='//input[@data-automation-id="radio-btn-bill-media-paper"]', element_name='Factura tiparita')
        self.utils.click_js(xpath='//button[@data-automation-id="summary-configuration-submit-btn"]', element_name='Trimite')

    def electronica(self):
        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="page-popup-section-content new-billing-email"]', element_name='Factura electronica') == True:
            self.utils.log_customStep(key='Verify that this is the default value selected', status='Passed')
            self.utils.click_js(xpath='//button[@data-automation-id="summary-configuration-submit-btn"]', element_name='Trimite')

        else:
            self.utils.log_customStep(key='Verify that this is the default value selected', status='Fail')
            self.utils.click_js(xpath='//input[@data-automation-id="radio-btn-bill-media-electronic"]', element_name='Buton de factura electronica')
            self.utils.click_js(xpath='//button[@data-automation-id="summary-configuration-submit-btn"]',element_name='Trimite')

    def checkDetalii(self):

        if self.utils.element_exists(enable_Logging=False, xPath='(//div[@class="radio-button-section"])[1]', element_name='Detalii factura') == True and self.utils.element_exists(enable_Logging=False, xPath='(//div[@class="radio-button-section"])[2]', element_name='Detalii factura') == True:
            self.utils.log_customStep(key='New address is displayed on the "Detalii" page and is selected', status='Passed')
        else:
            self.utils.log_customStep(key='New address is displayed on the "Detalii" page and is selected', status='Fail')


    def vdfLogo(self):
        self.utils.click_js(xpath='//img[@class="logo-vodafone-header"]', element_name='Logo Vdf')
        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup order-cancel-popup"]', element_name='Intrerupe comanda Pop-up') == True:
            self.utils.log_customStep(key='The "Intrerupe comanda" dialog is dispayed', status='Passed')
            lst = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            motiv = random.choice(lst)
            self.utils.dropDownSelector_Index(xpath='//select[@data-automation-id="select-reason-code"]', index=motiv, element_name='Motiv')
            self.utils.click_js(xpath='//button[@data-automation-id="cancel-popup-cancel-button"]', element_name='Strerge si iesi')

            self.utils.wait_loadingCircle()

            if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="subscribers-tabs"]', element_name='Dasbord Subscribers') == True:
                self.utils.log_customStep(key='The order is cancelled', status='Passed')
                self.utils.log_customStep(key='The user is redirected to the 360 dashboard', status='Passed')
            else:
                self.utils.log_customStep(key='The order is cancelled', status='Fail')
                self.utils.log_customStep(key='The user is redirected to the 360 dashboard', status='Fail')
        else:
            self.utils.log_customStep(key='The "Intrerupe comanda" dialog is dispayed', status='Fail')


    def backButton(self):
        self.utils.click_js(xpath='//img[@src="resources/assets/images/Back-icon.svg"]', element_name='BackButton')
        if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="wrap-popup order-cancel-popup"]', element_name='Intrerupe comanda Pop-up') == True:
            self.utils.log_customStep(key='The "Intrerupe comanda" dialog is dispayed', status='Passed')
            lst = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            motiv = random.choice(lst)
            self.utils.dropDownSelector_Index(xpath='//select[@data-automation-id="select-reason-code"]', index=motiv, element_name='Motiv')
            self.utils.click_js(xpath='//button[@data-automation-id="cancel-popup-cancel-button"]', element_name='Strerge si iesi')

            self.utils.wait_loadingCircle()

            if self.utils.element_exists(enable_Logging=False, xPath='//div[@class="subscribers-tabs"]', element_name='Dasbord Subscribers') == True:
                self.utils.log_customStep(key='The order is cancelled', status='Passed')
                self.utils.log_customStep(key='The user is redirected to the 360 dashboard', status='Passed')
            else:
                self.utils.log_customStep(key='The order is cancelled', status='Fail')
                self.utils.log_customStep(key='The user is redirected to the 360 dashboard', status='Fail')
        else:
            self.utils.log_customStep(key='The "Intrerupe comanda" dialog is dispayed', status='Fail')



    def cancelOrderNo(self,pageCustomerDetails_acquisitionType):
        if self.utils.element_exists(enable_Logging=False, xPath='//button[@class="footer-btn-primary"]', element_name='Continua Button'):
            self.utils.wait_loadingCircle(enable=True)
            if self.utils.element_isClickable(xpath='//button[@class="footer-btn-primary"]', element_name='Continua Button',enable_Logging=False):
                if pageCustomerDetails_acquisitionType == 'Portin postpaid':
                    if self.utils.element_exists(xPath='//input[@name="customerId"]', element_name='Cod Client Donor',enable_Logging=False):
                        self.utils.type_js(xpath='//input[@name="customerId"]', element_name='Cod Client Donor - Recheck if is mandatory due to DEX Error',value='', tab=True)
                        self.utils.wait_loadingCircle(enable=True)
                        time.sleep(10)
                        if self.utils.element_isClickable(xpath='//button[@class="footer-btn-primary"]',element_name='Continua Button - Recheck for Cod Client', enable_Logging=False):
                            self.utils.log_customStep(key='Cod Client Donor Field is NOT Mandatory',status='Fail')
                        else:
                            self.utils.log_customStep(key='Cod Client Donor Field is Mandatory',status='Passed')
                            self.utils.type_js(xpath='//input[@name="customerId"]',element_name='Cod Client Donor - New value', value='4321!@utomation2', tab=True)
                            self.utils.wait_loadingCircle(enable=True)

                self.utils.click_js(xpath='//button[@class="footer-btn-primary"]', element_name='Continua')
                self.utils.log_customStep(key='Continua Button - Enable', status='Passed')
            else:
                self.utils.log_customStep(key='!FAIL! - Continua Button is not Clickable, DEX Error', status='Fail')
        else:
            self.utils.log_customStep(key='Continua Button - Disable', status='Fail')







