from testTools.utilities import utilities as utils
#from dex_old.CBU_F_ACQ.PersTestModules import sebas, iulian


class page_Configureaza():

    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary=inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)

        self.pageConfigureaza_offerType = self.utils.get_Child(paramName='pageConfigureaza_offerType')
        self.pageConfigureaza_offerScenario = self.utils.get_Child(paramName='pageConfigureaza_offerScenario')
        self.pageConfigureaza_flowType = self.utils.get_Child(paramName='pageConfigureaza_flowType')
        self.pageConfigureaza_viewMore = self.utils.get_Child(paramName='pageConfigureaza_viewMore')
        self.pageConfigureaza_offerName = self.utils.get_Child(paramName='pageConfigureaza_offerName')
        self.pageConfigureaza_addOns = self.utils.get_Child(paramName='pageConfigureaza_addOns')
        self.pageConfigureaza_acquisitionType = self.utils.get_Child(paramName='pageConfigureaza_acquisitionType')
        self.pageConfigureaza_cartScenario = self.utils.get_Child(paramName='pageConfigureaza_cartScenario')
        self.pageConfigureaza_deviceStock = self.utils.get_Child(paramName='pageConfigureaza_deviceStock')
        self.pageConfigureaza_onDemandConsent = self.utils.get_Child(paramName='pageConfigureaza_onDemandConsent')
        self.pageConfigureaza_dotsActions = self.utils.get_Child(paramName='pageConfigureaza_dotsActions')
        self.pageConfigureaza_saveActions = self.utils.get_Child(paramName='pageConfigureaza_saveActions')
        self.pageConfigureaza_cancelActions = self.utils.get_Child(paramName='pageConfigureaza_cancelActions')
        self.pageConfigureaza_sendActions = self.utils.get_Child(paramName='pageConfigureaza_sendActions')

        self.testData = testData
#        self.sebas = sebas.testFunctions_Configureaza(self.utils)
#       self.iulian = iulian.testFunctions_Configureaza(self.utils)


    def test_page_Configureaza(self):

        self.utils.setTempValue('device_name', self.sebas.device['in_stock'])

        # print(self.utils.getTempValue('device_name'))

        # 1. pageConfigureaza_offerType / serviceAndDevice / viewMore - Testele existente nu intra pe aceasta ramura !
        # 2. pageConfigureaza_flowType / device - Redesign in CTDA on
             # Verify that, if (the channel is Retail and stock = 0) OR (the channel is Postpaid and stock = 0), the "Continua" button is disabled and the flow cannot continue
        # 3. pageConfigureaza_flowType / service / device / viewMore - device doesn't remain selected after page returns on configureaza from retail/devices
        # 4. pageConfigureaza_flowType = device / pageConfigureaza_deviceStock = onDemand - cheia "pageConfigureaza_deviceStock" apare de 2 ori in dictionar

        self.utils.log_customStep(
            key='WARN! No Tests for flow serviceAndDevice/viewMore -> CTDA Redesign"', status='FAIL')


        import random, time
        stay = random.uniform(3, 5)
        time.sleep(stay)

        # print(self.testData)
        self.sebas.page_validations()

        if self.pageConfigureaza_offerType == 'serviceOnly':
            self.sebas.configCommonSteps()
            self.sebas.svOnlySteps()

            # if self.pageConfigureaza_viewMore:
            #     pass
            # else:
            #     pass
            print(self.pageConfigureaza_viewMore)
            self.sebas.svOnlyViewMore(pageConfigureaza_offerName=self.pageConfigureaza_offerName, pageConfigureaza_viewMore=self.pageConfigureaza_viewMore)

            # self.sebas.svOnlyViewMore(pageConfigureaza_viewMore=self.pageConfigureaza_viewMore, pageConfigureaza_offerName=self.pageConfigureaza_offerName)

            # if self.pageConfigureaza_offerName == 'Discount':
            #     pass
            # elif self.pageConfigureaza_offerName == 'regularOffer':
            #     pass

        elif self.pageConfigureaza_offerType == 'serviceAndDevice':

            if self.pageConfigureaza_offerScenario == 'bundleSelection':
                self.sebas.bundleSteps(search_specific=True, pageConfigureaza_offerName=self.pageConfigureaza_offerName)
            elif self.pageConfigureaza_offerScenario == 'offerConfig':
                self.sebas.configCommonSteps()

                if self.pageConfigureaza_flowType == 'device':
                    self.utils.log_customStep(key='WARN! CTDA Redesign for flow Service2Device in case of stock = 0/stock = onDemand/PreOrder', status='FAIL')
                    self.sebas.DevtoServ()
                    if self.pageConfigureaza_viewMore == 'True':
                        self.sebas.DevtoServViewMore(pageConfigureaza_viewMore='False')
                        self.sebas.svOnlyViewMore(pageConfigureaza_viewMore='False',
                                                  pageConfigureaza_offerName=self.pageConfigureaza_offerName)
                        self.sebas.checkContinueStatus(enabled=False)
        ### !!!         self.sebas.stockBehaviour()
                    elif self.pageConfigureaza_viewMore == 'False':
                        # if self.pageConfigureaza_deviceStock == 'onDemand':
                        self.sebas.DevtoServViewMore(pageConfigureaza_viewMore='True')
                        self.sebas.svOnlyViewMore(pageConfigureaza_viewMore='True',
                                                  pageConfigureaza_offerName=self.pageConfigureaza_offerName)
                        self.sebas.checkContinueStatus(enabled=False)
        ### !!! Missing OnDemand device prereq
                        if self.pageConfigureaza_onDemandConsent:
                            pass
                        else:
                            self.sebas.RegularFlow()

                        # elif self.pageConfigureaza_deviceStock == 'regularFlow':
                        #     self.sebas.RegularFlow()

                elif self.pageConfigureaza_flowType == 'service':
                    # self.sebas.ServtoDev()
                    # print('svOnlyViewMore')
                    self.sebas.svOnlyViewMore(pageConfigureaza_viewMore=self.pageConfigureaza_viewMore, pageConfigureaza_offerName=self.pageConfigureaza_offerName)
                    # print('checkContinueStatus')
                    self.sebas.checkContinueStatus(enabled=False)
    ### !!!         self.sebas.stockBehaviour()
                    # print('DevtoServViewMore')
                    self.sebas.DevtoServViewMore(pageConfigureaza_viewMore=self.pageConfigureaza_viewMore)
                    self.utils.log_customStep(
                        key='WARN! Device is not selected after click on "SELECTEAZA" / only on AUTO', status='FAIL')
                    # print('checkContinueStatus')
                    self.sebas.checkContinueStatus(enabled=True)
                    # if self.pageConfigureaza_viewMore:
                    #     pass
                    # else:
                    #     pass

            elif self.pageConfigureaza_offerScenario == 'viewMore':
                self.sebas.viewMoreSteps(pageConfigureaza_offerName=self.pageConfigureaza_offerName)

                # self.sebas.selectOffer(pageConfigureaza_offerName=self.pageConfigureaza_offerName)

    ### AddOns

        if self.pageConfigureaza_addOns == 'changeSelection':
            self.iulian.changeSelection()
        elif self.pageConfigureaza_addOns == 'editAddOn':
            self.iulian.editAddOn()
        elif self.pageConfigureaza_addOns == 'noChange':
            self.iulian.noChange()



    ### Shopping Cart

        if self.pageConfigureaza_acquisitionType == 'generalAquisition' or self.pageConfigureaza_acquisitionType == 'MFP' or self.pageConfigureaza_acquisitionType == 'Portin postpaid' or self.pageConfigureaza_acquisitionType == 'Portin prepaid':
            self.iulian.acquisitionTypeCart(pageConfigureaza_acquisitionType=self.pageConfigureaza_acquisitionType)
            self.iulian.shoppingCartOffer(pageConfigureaza_offerType=self.pageConfigureaza_offerType)

        if self.pageConfigureaza_cartScenario == '3 Dots sub-menu':
            self.iulian.initialCheck()

            if self.pageConfigureaza_dotsActions == 'Anuleaza comanda':
                self.iulian.anuleaza()
                ### Dezvolatarea pe aceasta ramura a fost blocata (30.06) --> pop -ul nu aparea (conform documentatie/Schemei)   Defect ID #4016

                if self.pageConfigureaza_cancelActions == 'Anuleaza comanda':
                    pass
                elif self.pageConfigureaza_cancelActions == 'Continua comanda':
                    pass

            elif self.pageConfigureaza_dotsActions == 'Salveaza':
                self.iulian.salveaza()

                if self.pageConfigureaza_saveActions == 'Anuleaza':
                    self.iulian.salveazaAnuleaza()
                elif self.pageConfigureaza_saveActions == 'Salveaza si inchide':
                    # self.utils.get_text(xPath='//div[@class="top-texts"]',element_name='Subscriber name from C360 (Dashboard)')
                    # DE Adaugat verificareDashboard!! SebiJR -- am folosit CNP ul pentru 'verificare'
                    self.iulian.salveazaInchide(verificareDashboard=self.utils.getTempValue('cnp_dash'))

            elif self.pageConfigureaza_dotsActions == 'Trimite mail':
                self.iulian.trimiteMailCommon()

                if self.pageConfigureaza_sendActions == 'Trimite':
                    self.iulian.trimiteMail()
                elif self.pageConfigureaza_sendActions == 'Vizualizeaza':
                    self.iulian.vizualizeazaMail()

            elif self.pageConfigureaza_dotsActions == 'Vizualizeaza':
                self.iulian.vizualizeaza()


        elif self.pageConfigureaza_cartScenario == 'Mini-cart menu':
            #de editat metoda!!!
            # Click on the flow type button - reprezinta butonul de la 'numar nou' din shopping cart
            self.iulian.miniCart()

        elif self.pageConfigureaza_cartScenario == 'Continue button':
            self.iulian.continuaButton()





# configureaza = page_Configureaza(inputDictionary={}, browser='c', testName='test_page_Configureaza', testData=[])
# configureaza.test_page_Configureaza()