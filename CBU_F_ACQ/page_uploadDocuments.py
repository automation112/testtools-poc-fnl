from testTools.utilities import utilities as utils
from .PersTestModules import sebas


class page_uploadDocuments():

    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary=inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)

        self.pageUploadDocuments_docOpt = self.utils.get_Child(paramName='pageUploadDocuments_docOpt')
        self.pageUploadDocuments_IDconsent = self.utils.get_Child(paramName='pageUploadDocuments_IDconsent')
        self.pageUploadDocuments_signatureMethod = self.utils.get_Child(paramName='pageUploadDocuments_signatureMethod')
        self.pageUploadDocuments_electronicMethod = self.utils.get_Child(paramName='pageUploadDocuments_electronicMethod')

        self.testData = testData
        self.sebas = sebas.testFunctions_uploadDocuments(self.utils)


    def test_page_uploadDocuments(self):

        # 1. Missing section "In the ID Storage Preferences, the user can choose if Vodafone stores the documents or not"
        # 2. Missing section "Click in the "CI | Pasaport" section to upload the documents"
        # 3. on DEX2 - Error page is loaded after clicking "INCARCA DOCUMENTE" - added WA on docSteps
        # Defect opened for 1 & 2

        import random, time
        stay = random.uniform(3, 5)
        time.sleep(stay)
        # print('waited', stay, 'seconds')

        self.utils.log_customStep(key='WARN! Missing sections : ID Storage Preferences / CI | Pasaport ... Defect ID # : ', status='FAIL')

        self.sebas.page_checks()

        if self.pageUploadDocuments_docOpt == 'firstOption':
            self.sebas.docSendFirstOp()
        elif self.pageUploadDocuments_docOpt == 'secondOption':
            self.sebas.docSendSecondOp()

    # Missing Fields
        if self.pageUploadDocuments_IDconsent == 'no':
            print('Missing Field _ No')
        elif self.pageUploadDocuments_IDconsent == 'yes':
            print('Missing Field _ Yes')
    # Missing Fields

        if self.pageUploadDocuments_signatureMethod == 'No':
            self.sebas.paperSignature()
        elif self.pageUploadDocuments_signatureMethod == 'Yes':
            self.sebas.electronicSignature(pageUploadDocuments_electronicMethod=self.pageUploadDocuments_electronicMethod)

        time.sleep(3)
        self.sebas.docSteps(pageUploadDocuments_docOpt=self.pageUploadDocuments_docOpt, clickIncarcaDoc='No')

