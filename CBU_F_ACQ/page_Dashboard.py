from collections import defaultdict

from testTools.utilities import utilities as utils
#from dex_old.CBU_F_ACQ.PersTestModules import sebid, iulian
import time as t

class page_Dashboard():

    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary=inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)

        self.configType = self.utils.get_Child(paramName='configType')
        self.abNouType = self.utils.get_Child(paramName='abNouType')
        self.tipServiciu = self.utils.get_Child(paramName="tipServiciu")
        self.retention_option = self.utils.get_Child(paramName="retention_option")
        self.retention_flow = self.utils.get_Child('retention_flow')
        self.pageConfigureaza_acquisitionType = self.utils.get_Child(paramName='pageConfigureaza_acquisitionType')
        self.pageLogin_prerequisite = self.utils.get_Child(paramName='pageLogin_prerequisite')
        self.scenarioNameDC = self.utils.get_Child(paramName='scenarioNameDC')
        self.testData = testData
        #self.sebi = sebid.testFunctions_Dashboard(self.utils)
        #self.iulian = iulian.testFunctions_Dashboard(self.utils)
        self.dasboardDict = {'Abonament_nou': "//div[@data-automation-id='new-subscriber-link']",
                             'Numar_Nou(GA)': '//li[@data-automation-id="tab-key-newWirelessNumber-0"]',
                             'Credit_Rate_Da': '//label[1]//*[@name="isCustomerConsent"]',
                             'Credit_Rate_Nu': '//label[2]//*[@name="isCustomerConsent"]',
                             'Numar_Contact': '//input[@name="contactNumber"]',
                             'continua': "//button[contains(@class,'primary bold right-align')]",
                             'Portare': '//li[@data-automation-id="tab-key-portIn-1"]',
                             'Numar_Portat': '//input[@name="portedNumber"]',
                             'Donor': '//select[@name="provider"]',
                             'Numar_Contact_Portare': "//div[2]/div[@class='fields-wrap' and 1]/div[1]/input[1]",
                             'POSTPAID': '//input[@name="portedNumberType" and @value="postpaid"]',
                             'Prepaid': '//input[@name="portedNumberType" and @value="prepaid"]',
                             'Migrare_de_la_Prepaid': "//li[@data-automation-id='tab-key-migrateToPostpaid-2']",
                             'Number_To_Migrate': '//input[@name="numberToMigrate"]',
                             'Trimtie Token': "//span[contains(text(),'TRIMITE TOKEN')]",
                             'Introdu Token': "//input[@placeholder='Introdu TOKEN']",
                             'Valideaza Token': """//a[@data-automation-id="token-consent-validate-token"]""",
                             'Factura_1': "//input[@name='invoice1']",
                             'Factura_2': "//input[@name='invoice2']",
                             'Factura_3': "//input[@name='invoice3']",
                             'adauga': "//button[contains(@class,'bold nbo-button')]",
                             'BundleBannerOffer': "//span[contains(text(),'Împreuna cu')]",
                             '360_OferteMobile': """//span[contains(text(),'OFERTE MOBILE')]""",
                             '360_Doar_Servicii': "//span[contains(text(),'DOAR SERVICII')]",
                             }


    def test_page_Dashboard(self):
        if '_ACHIZITIE' in self.configType:
            self.utils.wait_loadingCircle(enable=True)
            # self.browser.find_element_by_xpath("//div[@data-automation-id='new-subscriber-link']").send_keys(Keys.PAGE_DOWN)
            #t.sleep(10)
            self.utils.click_js(self.dasboardDict['Abonament_nou'])
            print("TESTTTTTTTTTTTTTTTTTTTTT")
            self.utils.wait_loadingCircle()
            if self.abNouType == 'GA':
                t.sleep(0.5)
                self.utils.click_js(self.dasboardDict['Numar_Nou(GA)'])

                if self.scenarioNameDC == 'CR_True_Client_Matur':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                    # self.utils.click_js( """//input[@name="isCustomerConsent"][1]""").click()

                elif self.scenarioNameDC == 'CR_False_Client_Matur':
                    self.utils.logScenariu('click Credit Rate = "Nu"')
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()
                    count = 3

                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                    else:
                        tokenId = self.utils.retryAPI(API='Token',
                                                                URL='http://devops03.connex.ro:8030/getSMSToken/',
                                                                KEY=self.utils.getTempValue(
                                                                    'brokenMsisdn'))
                        self.utils.wait_loadingCircle()
                        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                        self.utils.click_js(
                            xpath="""//a[@data-automation-id="token-consent-validate-token"]""")
                        self.utils.wait_loadingCircle()

                        if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
                            self.utils.log_customStep('VALID TOKEN', 'FAIL')
                        else:
                            self.utils.log_customStep('VALID TOKEN', 'PASSED')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    count = 3
                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                    else:
                        tokenId = self.utils.retryAPI(API='Token',
                                                                URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                                KEY=self.utils.getTempValue(
                                                                    'brokenMsisdn'))
                        self.utils.wait_loadingCircle()

                        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    count = 3
                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    for i in range(1, 6):
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                        count = 3
                        while self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                        if self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep(
                                'Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i), 'FAIL')

                        if i >= 2 and i < 6:
                            if self.utils.element_exists(
                                    xPath="//div[@class='attempts-remaining']") == True:
                                self.utils.log_customStep('Incercari ramase Displayed', 'PASSED')
                            else:
                                self.utils.log_customStep('Incercari ramase Displayed', 'FAIL')

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep(
                            'Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i),
                            'FAIL')
                    else:
                        if self.utils.element_exists(
                                "//div[@class='token-validate-block']//button") == True:
                            self.utils.log_customStep('Trimite Token isDisabled', "PASSED")
                        else:
                            self.utils.log_customStep('Trimite Token isDisabled', "FAIL")

                        tokenId = self.utils.retryAPI(API='Token',
                                                                URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                                KEY=self.utils.getTempValue(
                                                                    'brokenMsisdn'))

                        self.utils.wait_loadingCircle()
                        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                        self.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                        if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
                            self.utils.log_customStep('VALID TOKEN', 'FAIL')
                        else:
                            self.utils.log_customStep('VALID TOKEN', 'PASSED')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_TokenIgnored':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                elif self.scenarioNameDC == 'CR_False_Client_nonMatur':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                t.sleep(10)
                self.utils.click_js(self.dasboardDict['continua'])

            elif self.abNouType == 'MNP':
                t.sleep(0.5)
                contact = '734953572'
                ported_no = '730044448'
                self.utils.setTempValue('ported_no', ported_no)

                self.utils.logScenariu('Click pe Portare')
                self.utils.click_js(self.dasboardDict['Portare'])
                self.utils.type_js(xpath=self.dasboardDict['Numar_Contact_Portare'], value=contact)
                self.utils.type_js(xpath=self.dasboardDict['Numar_Portat'], value=ported_no, enter='yes')
                self.utils.dropDownSelector_Index(self.dasboardDict['Donor'], 1)

                if self.tipServiciu == "POSTPAID":
                    self.utils.click_js(self.dasboardDict['POSTPAID'])
                elif self.tipServiciu == "PREPAID":
                    self.utils.click_js(self.dasboardDict['Prepaid'])

                if self.scenarioNameDC == 'CR_True_Client_Matur':
                    self.utils.logScenariu('click Credit Rate = "Da"')
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                elif self.scenarioNameDC == 'CR_False_Client_Matur':
                    self.utils.logScenariu('click Credit Rate = "Nu"')
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    if self.tipServiciu == 'POSTPAID':
                        self.utils.type_js(self.dasboardDict['Factura_1'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_2'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_3'], '300')

                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    count = 3
                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                    else:
                        tokenId = self.utils.retryAPI(API='Token',
                                                                URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                                KEY=self.utils.getTempValue(
                                                                    'brokenMsisdn'))
                        self.utils.wait_loadingCircle()
                        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)
                        self.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                    if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
                        self.utils.log_customStep('VALID TOKEN', 'FAIL')
                    else:
                        self.utils.log_customStep('VALID TOKEN', 'PASSED')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                    if self.tipServiciu == 'POSTPAID':
                        self.utils.type_js(self.dasboardDict['Factura_1'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_2'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_3'], '300')

                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    count = 3
                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                    else:
                        tokenId = self.utils.retryAPI(API='Token',
                                                                URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                                KEY=self.utils.getTempValue(
                                                                    'brokenMsisdn'))
                        self.utils.wait_loadingCircle()

                        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':
                    if self.tipServiciu == 'POSTPAID':
                        self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        self.utils.type_js(self.dasboardDict['Factura_1'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_2'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_3'], '300')

                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    count = 3
                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')


                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':

                    if self.tipServiciu == 'POSTPAID':
                        self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                        self.utils.type_js(self.dasboardDict['Factura_1'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_2'], '300')
                        self.utils.type_js(self.dasboardDict['Factura_3'], '300')
                    for i in range(1, 6):

                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                        count = 3
                        while self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                        if self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep(
                                'Max4Tkn_Count_{}_Trimite token fail after Retry'.format(i), 'FAIL')

                        if i >= 2 and i < 6:
                            if self.utils.element_exists(
                                    xPath="//div[@class='attempts-remaining']") == True:
                                self.utils.log_customStep('Incercari ramase Displayed', 'PASSED')
                            else:
                                self.utils.log_customStep('Incercari ramase Displayed', 'FAIL')

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry',
                                                            'FAIL')
                    else:
                        if self.utils.element_exists(
                                "//div[@class='token-validate-block']//button") == True:
                            self.utils.log_customStep('Trimite Token isDisabled', "PASSED")
                        else:
                            self.utils.log_customStep('Trimite Token isDisabled', "FAIL")

                        tokenId = self.utils.retryAPI(API='Token',
                                                                URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                                KEY=self.utils.getTempValue(
                                                                    'brokenMsisdn'))

                        self.utils.wait_loadingCircle()
                        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                        self.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")
                        if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
                            self.utils.log_customStep('VALID TOKEN', 'FAIL')
                        else:
                            self.utils.log_customStep('VALID TOKEN', 'PASSED')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_TokenIgnored':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                elif self.scenarioNameDC == 'CR_False_Client_nonMatur':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                    #
                    # if self.tipServiciu == 'POSTPAID':
                    #     self.utils.type_js(self.dasboardDict['Factura_1'], '300')
                    #     self.utils.type_js(self.dasboardDict['Factura_2'], '300')
                    #     self.utils.type_js(self.dasboardDict['Factura_3'], '300')

                self.utils.click_js(self.dasboardDict['continua'])

            elif self.abNouType == 'MFP':
                t.sleep(0.5)
                # pre_msisdn = self.utils.retryAPI(API='Prepaid', URL=self.utils.getTempValue('URL')['getPrepaid'])
                pre_msisdn = 726983525
                self.utils.logScenariu('MIGRARE DE LA PREPAID')
                self.utils.click_js(self.dasboardDict['Migrare_de_la_Prepaid'])

                if self.scenarioNameDC == 'CR_True_Client_Matur':
                    print("TESTTTTTTTTTTTTTTTTT123TTTT")
                    print("TESTTTTTTTTTTTTTT123TTTTTTT")

                    self.utils.logScenariu('click Credit Rate = "Da"')
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                elif self.scenarioNameDC == 'CR_False_Client_Matur':

                    self.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_Yes':
                    # print('im here')
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    count = 3
                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                    else:

                        tokenId = self.utils.retryAPI(API='Token',
                                                                URL=self.utils.getTempValue('URL')['getSMSToken'],
                                                                KEY=self.utils.getTempValue(
                                                                    'brokenMsisdn'))

                        self.utils.wait_loadingCircle()

                        self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']", value=tokenId)

                        self.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                        if self.utils.element_exists(xPath="//div[@class='error-text']") == True:

                            self.utils.log_customStep('VALID TOKEN', 'FAIL')

                        else:

                            self.utils.log_customStep('VALID TOKEN', 'PASSED')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_ValidateToken_No':

                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_Token_Fall_through':

                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                    self.utils.wait_loadingCircle()

                    count = 3
                    while self.utils.element_exists(
                            "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                        count -= 1
                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')


                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_max4TokensSent':

                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])

                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                    for i in range(1, 6):

                        self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                        self.utils.wait_loadingCircle()

                        count = 3
                        while self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]") == True and count >= 0:
                            count -= 1
                            self.utils.click_js(xpath="//span[contains(text(),'TRIMITE TOKEN')]")
                            self.utils.wait_loadingCircle()

                        if self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep(
                                'Max4Tkn_{}_Trimite token fail after Retry'.format(i), 'FAIL')

                        if i >= 2 and i < 6:

                            if self.utils.element_exists(
                                    xPath="//div[@class='attempts-remaining']") == True:

                                self.utils.log_customStep('Incercari ramase Displayed', 'PASSED')

                            else:

                                self.utils.log_customStep('Incercari ramase Displayed', 'FAIL')

                    if self.utils.element_exists("//span[contains(text(),'Trimiterea Token-ului a e')]"):
                        self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                    else:

                        if self.utils.element_exists(
                                "//div[@class='token-validate-block']//button") == True:

                            self.utils.log_customStep('Trimite Token isDisabled', "PASSED")

                        else:

                            self.utils.log_customStep('Trimite Token isDisabled', "FAIL")

                        if self.utils.element_exists(
                                "//span[contains(text(),'Trimiterea Token-ului a e')]"):
                            self.utils.log_customStep('Trimite token fail after Retry', 'FAIL')
                        else:
                            tokenId = self.utils.retryAPI(API='Token',
                                                                    URL=self.utils.getTempValue('URL')[
                                                                        'getSMSToken'],
                                                                    KEY=self.utils.getTempValue(
                                                                        'brokenMsisdn'))

                            # print('This is the token', tokenId,  'this the shiet', self.utils.getTempValue('brokenMsisdn'))

                            self.utils.wait_loadingCircle()

                            self.utils.type_js(xpath="//input[@placeholder='Introdu TOKEN']",
                                                         value=tokenId)

                            self.utils.click_js(xpath="//label[contains(text(),'VALIDEAZ')]")

                            if self.utils.element_exists(xPath="//div[@class='error-text']") == True:
                                self.utils.log_customStep('VALID TOKEN', 'FAIL')
                            else:
                                self.utils.log_customStep('VALID TOKEN', 'PASSED')

                elif self.scenarioNameDC == 'CR_True_Client_nonMatur_TokenIgnored':
                    self.utils.click_js(self.dasboardDict['Credit_Rate_Da'])
                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')
                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                elif self.scenarioNameDC == 'CR_False_Client_nonMatur':

                    self.utils.click_js(self.dasboardDict['Credit_Rate_Nu'])

                    self.utils.logScenariu('Type Numar Contact')

                    self.utils.type_js(xpath=self.dasboardDict['Numar_Contact'], value='730963536')

                    self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)

                # self.utils.type_js(xpath=self.dasboardDict['Number_To_Migrate'], value=pre_msisdn)
                self.utils.wait_loadingCircle()
                t.sleep(1.5)
                self.utils.click_js(self.dasboardDict['continua'])

        elif '_RETENTIE' in self.configType:
            print('retentie ++')
            print(self.retention_option, self.retention_flow)
            if self.retention_option == '360_BannerOffer':

                if self.retention_flow == 'BannerOffer_Service_and_Device':
                    print('bundle banner offer')

                    self.utils.setTempValue('BannerType', 'Service_and_Device')

                    # devAndServiceXpaths = {'Device_Info': ["//div[@class='banner-line'][2]//span[@class='hero-device-banner-2'][{}]"],
                    #                        'Rate': ["//div[@class='banner-line'][3]//span[@class='hero-device-banner-4'][{}]"],
                    #                        'Service_Info': ["//div[@class='banner-line'][5]//span[@class='hero-device-banner-3'][{}]", "//div[@class='banner-line'][5]//span[@class='hero-device-banner-2'][{}]"]
                    #                        }

                    devAndServiceXpaths = {
                        'Device_Info': '//div[@class="banner-line"][2]',
                        'Rate_Info': '//div[@class="banner-line"][3]',
                        'Service_Info': '//div[@class="banner-line"][5]'
                    }

                    collectInfoDict = defaultdict(dict)

                    for key, value in devAndServiceXpaths.items():
                        txtVal = self.utils.get_text(value)
                        collectInfoDict[key] = txtVal
                        self.utils.log_customStep('Banner_{}'.format(key),
                                                            'PASSED --> [{}]'.format(txtVal.replace('ă', '')))

                    self.utils.setTempValue('BannerInfo', dict(collectInfoDict))
                    print('collection dict = ', collectInfoDict)
                    self.utils.click_js(self.dasboardDict['adauga'], priority='CRITICAL')

                elif self.retention_flow == 'BannerOffer_ServiceOnly':
                    self.utils.setTempValue('BannerType', 'ServiceOnly')

                    devAndServiceXpaths = {
                        'Service_Info': [
                            "//div[@class='banner-line'][5]//span[@class='hero-device-banner-3'][{}]",
                            "//div[@class='banner-line'][5]//span[@class='hero-device-banner-2'][{}]"]
                    }

                    collectInfoList = defaultdict(dict)

                    for i in list(range(1, 7)):
                        for key, value in devAndServiceXpaths.items():
                            collectInfoList[key] = []
                            for h in value:
                                textValue = self.utils.get_text(h.format(i), timeout=False)
                                if textValue != False:
                                    collectInfoList[key].append(textValue.strip())

                    self.utils.setTempValue('BannerInfo', dict(collectInfoList))
                    self.utils.click_js(self.dasboardDict['adauga'], priority='CRITICAL')

                elif self.retention_flow == 'BannerOffer_DeviceOnly':
                    self.utils.setTempValue('BannerType', 'DeviceOnly')

                    devAndServiceXpaths = {
                        'Device_Info': ["//div[@class='banner-line'][2]//span[@class='hero-device-banner-2'][{}]"],
                        'Rate': ["//div[@class='banner-line'][3]//span[@class='hero-device-banner-4'][{}]"],
                    }

                    collectInfoList = defaultdict(dict)

                    for i in list(range(1, 7)):
                        for key, value in devAndServiceXpaths.items():
                            collectInfoList[key] = []
                            for h in value:
                                textValue = self.utils.get_text(h.format(i), customTimeout=0.1)
                                if textValue != False:
                                    collectInfoList[key].append(textValue.strip())

                    self.utils.setTempValue('BannerInfo', dict(collectInfoList))
                    self.utils.click_js(self.dasboardDict['adauga'], priority='CRITICAL')

            elif self.retention_option == '360_Doar_Servicii':
                if self.retention_flow == 'Service_only':
                    self.utils.click_js("//span[contains(text(),'DOAR SERVICII')]")

            elif self.retention_option == '360_OFERTE_MOBILE':
                if self.retention_flow == 'ConfigureazaOferte':
                    print('before click print')
                    self.utils.click_js("//span[contains(text(),'OFERTE MOBILE')]")
                elif self.retention_flow == 'PachetPromo':
                    print('sunt pe pachete promo')
                    self.utils.click_js("//span[contains(text(),'OFERTE MOBILE')]")


