from testTools.utilities import utilities as utils
#from dex_old.CBU_F_ACQ.PersTestModules import sebid
import time

class page_Login():

    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary=inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)

        self.channel = self.utils.get_Child(paramName='channel')
        self.testData = testData
        #self.sebi = sebid.testFunctions_Login(self.utils)
        # self.iulian = iulian.testFunctions_Configureaza(self.utils)


    def test_page_Login(self):

        self.login_dict = {'OK': '//*[@id="root"]/div[3]/div/button',
                       'UserName': '//input[@name="username"]',
                       'Password': '//input[@name="password"]',
                       'Login': '//button[@data-automation-id="login-button"]',
                       'DealerSelect': "//select[@data-automation-id='select-dealer']",
                       'Continua': "//button[@data-automation-id='dealer-login-button']"}

        if self.channel == 'RETAIL':
            time.sleep(5)
            # Click ok
            self.utils.click_js(xpath=self.login_dict['OK'])
            # Find username and insert user
            self.utils.type_js(self.login_dict['UserName'], 'florin.dedu')
            # Find password and insert pass
            self.utils.type_js(self.login_dict['Password'], 'abc123')
            self.utils.click_js(self.login_dict['Login'], priority='CRITICAL')
            # self.utils.wait_loadingCircle()
            # Search dealer(1) on page and click continue
            self.utils.dropDownSelector_Index(self.login_dict['DealerSelect'], 1)
            # self.utils.log_customStep('Test', 'FAIL')
            self.utils.click_js(self.login_dict['Continua'], priority='CRITICAL')

        elif self.channel == 'TELESALES':
            # Click ok
            self.utils.click_js(self.login_dict['OK'])
            self.utils.type_js(self.login_dict['UserName'], 'cm.test1')
            # Find password and insert pass
            self.utils.type_js(self.login_dict['Password'], 'abc123')
            self.utils.click_js(self.login_dict['Login'])
