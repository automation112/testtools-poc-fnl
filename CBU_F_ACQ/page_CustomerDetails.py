from testTools.utilities import utilities as utils
#from dex_old.CBU_F_ACQ.PersTestModules import iulian


class page_CustomerDetails():

    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary=inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)

        self.pageCustomerDetails_acquisitionType = self.utils.get_Child(paramName='pageCustomerDetails_acquisitionType')
        self.pageCustomerDetails_portinType = self.utils.get_Child(paramName='pageCustomerDetails_portinType')
        self.pageCustomerDetails_tempType = self.utils.get_Child(paramName='pageCustomerDetails_tempType')
        self.pageCustomerDetails_permanent_phoneType = self.utils.get_Child(paramName='pageCustomerDetails_permanent_phoneType')
        self.pageCustomerDetails_existingNotifNo = self.utils.get_Child(paramName='pageCustomerDetails_existingNotifNo')
        self.pageCustomerDetails_notifAction = self.utils.get_Child(paramName='pageCustomerDetails_notifAction')
        self.pageCustomerDetails_userType = self.utils.get_Child(paramName='pageCustomerDetails_userType')
        self.pageCustomerDetails_newBilling = self.utils.get_Child(paramName='pageCustomerDetails_newBilling')
        self.pageCustomerDetails_existingStreet = self.utils.get_Child(paramName='pageConfigureaza_onDemandConsent')
        self.pageCustomerDetails_invoiceType = self.utils.get_Child(paramName='pageCustomerDetails_invoiceType')
        self.pageCustomerDetails_cancelOrder = self.utils.get_Child(paramName='pageCustomerDetails_cancelOrder')
        self.pageCustomerDetails_existingSIM = self.utils.get_Child(paramName='pageCustomerDetails_existingSIM')
        self.pageCustomerDetails_existingSIM = self.utils.get_Child(paramName='pageCustomerDetails_existingSIM')
        self.pageCustomerDetails_cancelType = self.utils.get_Child(paramName='pageCustomerDetails_cancelType')
        self.pageCustomerDetails_minor = self.utils.get_Child(paramName='pageCustomerDetails_minor')
        self.pageSearch_mainScenario = self.utils.get_Child(paramName='pageSearch_mainScenario')
        self.pageCustomerDetails_temporary_phoneType = self.utils.get_Child(paramName='pageCustomerDetails_temporary_phoneType')
        self.pageShippingMethods_existingSIM = self.utils.get_Child(paramName='pageShippingMethods_existingSIM')
        self.pageOrderSummary_userActions = self.utils.get_Child(paramName='pageOrderSummary_userActions')



        self.testData = testData
      #  self.iulian = iulian.testFunctions_CustomerDetails(self.utils)


    def test_page_CustomerDetails(self):


        # self.iulian.customerDetails(detaliiClient_pgCD=self.iulian.detaliiClient(), detaliiClient_pgD=self.get) --> voi face o metoda de verifica 2 dictionare


        if self.pageSearch_mainScenario == 'newCustomer':
            self.iulian.newCustomer()

        self.iulian.detaliiClient()

        self.iulian.customerDetails()

        if self.pageCustomerDetails_acquisitionType == 'generalAquisition':
            self.iulian.generalAquisition()
        elif self.pageCustomerDetails_acquisitionType == 'MFP':

            if self.pageCustomerDetails_existingSIM == 'No':
                pass
            elif self.pageCustomerDetails_existingSIM == 'Yes':
                self.iulian.MFP_YES()

        elif self.pageCustomerDetails_acquisitionType == 'Portin postpaid' or self.pageCustomerDetails_acquisitionType == 'Portin prepaid':

            self.iulian.acquisitionType(pageCustomerDetails_acquisitionType = self.pageCustomerDetails_acquisitionType)

            if self.pageCustomerDetails_portinType == 'permanent':
                self.iulian.permanent()


                if self.pageCustomerDetails_permanent_phoneType == 'Customized':
                    self.iulian.customized()
                elif self.pageCustomerDetails_permanent_phoneType == 'Random':
                    self.iulian.random()

            elif self.pageCustomerDetails_portinType == 'temporary':

                if self.pageCustomerDetails_tempType == 'No':
                    self.iulian.temporaryNo()
                elif self.pageCustomerDetails_tempType == 'Yes':
                    self.iulian.temporaryYes()

                    if self.pageCustomerDetails_temporary_phoneType == 'Customized':
                        self.iulian.customized()
                    elif self.pageCustomerDetails_temporary_phoneType == 'Random':
                        self.iulian.random()

            if self.pageCustomerDetails_existingNotifNo == 'False':
                self.iulian.existingNotifNo()

            elif self.pageCustomerDetails_existingNotifNo == 'True':

                if self.pageCustomerDetails_notifAction == 'Anuleaza':
                    self.iulian.notifActionAnuleaza()
                elif self.pageCustomerDetails_notifAction == 'X':
                    self.iulian.notifActionX()



        if self.pageCustomerDetails_userType == 'No':
            self.iulian.userTypeNo()

            if self.pageCustomerDetails_minor == 'Yes':
                self.iulian.minorYes()

            elif self.pageCustomerDetails_minor == 'No':
                self.iulian.minorNo()

        elif self.pageCustomerDetails_userType == 'Yes':
            self.iulian.userTypeYes()

        if self.pageCustomerDetails_newBilling == 'No':
            pass


        elif self.pageCustomerDetails_newBilling == 'Yes':
            self.iulian.newBillingYes()

            if self.pageCustomerDetails_existingStreet == 'No':
                self.iulian.existingstreetYN()

            elif self.pageCustomerDetails_existingStreet == 'Yes':
                self.iulian.existingstreetYN()

                if self.pageCustomerDetails_invoiceType == 'Tiparire':
                    self.iulian.tiparire()

                elif self.pageCustomerDetails_invoiceType == 'Electronica':
                    self.iulian.electronica()

                self.iulian.checkDetalii()

        if self.pageCustomerDetails_cancelOrder == 'Yes':

            if self.pageCustomerDetails_cancelType == 'backButton':
                self.iulian.backButton()

            elif self.pageCustomerDetails_cancelType == 'VDFLogo':
                self.iulian.vdfLogo()

        elif self.pageCustomerDetails_cancelOrder == 'No':
            self.iulian.cancelOrderNo(pageCustomerDetails_acquisitionType=self.pageCustomerDetails_acquisitionType)



