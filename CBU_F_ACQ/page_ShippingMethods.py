from testTools.utilities import utilities as utils
from dex_old.CBU_F_ACQ.PersTestModules import sebid


class page_ShippingMethods():
    # flow-ul de device nu a putut fi facut din cauza indisponibilitatii de stoc din dex
    def __init__(self, inputDictionary, browser, testName, testData):
        self.input_Dictionary = inputDictionary
        self.utils = utils(callingObj=self, browser=browser, inputDictionary=inputDictionary, testCaseName=testName)
        self.pageShippingMethods_existingSIM = self.utils.get_Child(paramName="pageShippingMethods_existingSIM")
        self.testData = testData
        self.sebi = sebid.TestFunctionsShipp(self.utils)

    def test_page_ShippingMethods(self):
        self.sebi.MainScene()